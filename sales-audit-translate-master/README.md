# Process Description
This process will monitor tables for unprocessed json data and convert it into xml
The json data comes from manhattan point of sale. 
When a transaction is created on a store register, that transaction is sent to amazon web services (AWS SQS). 
Another process is in place that will pull that transaction off of AWS SQS and place it in the tables that this process monitors.
This process will convert the transaction's json data into xml data that can be used by sales audit and place it back into the same table record it came from.
The table names are defined in the .env environment file in this process root directory

## Application Deployment

    process root directory: 
        /home/vagrant/bin/sales-audit-translate/

    process entry point: 
        /home/vagrant/bin/sales-audit-translate/translate

    process log directory:
        create log directory:
            mkdir /var/log/sales-audit-translate/
        give all access to logs:
            chmod 777 /var/log/sales-audit-translate/

    Call process:
        A symbolic link was created in the /usr/bin directory so that the process can be called globaly from anywhere in the system by typing the command "translate"
    
        Command: translate

        Create soft link command: 
            ln -s /home/vagrant/bin/sales-audit-translate/translate /usr/bin/translate

    fix executable: if the symbolic link has an issue with calling the program it might be fixed by this command
        sed -i -e 's/\r$//' translate

    fix translate file for unix format
        Fixed it with these commands in vim:
        vi 
        Escape 
        :set fileformat=unix
        :wq!

    Table Journaling: 
        the tables used in this process must be journaling for commitment control
        commitment control is needed to prevent two processes from accessing the same table record at the same time
        two instences of the same process will be running simultaniously due to active/active system architecture. 
        active/active: two systems both active and running the same processes simultaniously

        create journal receiver:
            CRTJRNRCV JRNRCV(JSONDTALIB/POSJRNRCV) THRESHOLD(1000000000)
        create journal:
            CRTJRN JRN(JSONDTALIB/POSJRN) JRNRCV(JSONDTALIB/POSJRNRCV)
        start table journaling on tables needed:   
            STRJRNPF FILE(JSONDTALIB/POSRBKOFTL) JRN(JSONDTALIB/POSJRN)
            STRJRNPF FILE(JSONDTALIB/POSRSLSXTL) JRN(JSONDTALIB/POSJRN)
            STRJRNPF FILE(JSONDTALIB/POSSQSHSTL) JRN(JSONDTALIB/POSJRN)
    
    Enviornment 


## Additional Documentation
    S:\Projects\POS - Manhattan\Register Transaction Processing

--

## Transaction Documentation

### Transaction number:
    The standard transaction number generated in Point of Sale system has 21 digits, with the following details and format-TTMMDDYYSSSSRRRNNNNMC, where in:

    1. TT: It is the number that signifies the transaction type.  Active Omni has a centralized transaction number generation logic that caters to all types of transactions originating from the store.  The various types of transactions handled are:
        1. Sale/Return/Exchange order
        2. Manager function transactions like Pay In, Pay Out, Till Pick Up, Till Loan, No Sale.
        3. Post Void Sale or Return Order
        4. Post Void manager function transaction
        5. Gift Card related transactions like Reload, Activation, Balance Enquiry.
        6. Selling more items within a Pick Up at store order.
    2. MMDDYY is the date when the transaction is captured.
    3. SSSS is the Store Number from where the transaction is originated.  For transaction number generation, one must define the store location with 4 digits as the display ID.
    4. RRR is the Register Number where the transaction occurred.   User must set up the Register with 3 digits only.
    5. NNNN is the next step generated based on the sequence defined.  Each register in a store can have up to 9999 unique transactions created in a day.  
    6. M is the deployment mode in which the transaction is created.  0 indicates online, 1- edge mode.
    7. C is the Check digit number for the bar code.

### Creation of Transactions:
    The creation of transactions follows the below order in xml tag creation:
    1.  <POS>
    2.  Main Info
        a. Operator
        b. Work Station
        c. Till Number
        d. Transaction Number
        e. ID
        f. Store Number
        g. Time
        h. Order ID
    3.  <Trade>
        a. <Customer>
        b. <AR Customer Details>
        c. <Rewards>
        d. <AR Purchaser Info>
        e. <Items>
        f. <Tax>
        g. <Tender>

### Types of Discounts:
    1.  Item $ Discount
    2.  Item % Discount
    3.  Transaction $ Discount
    4.  Transaction % Discount
    5.  Rewards Discount
    6.  Coupon Discount
    7.  Tax Exempt

### Tender Types:
    1.  Cash
    2.  Cash with Change
    3.  Check
    4.  A/R Accounts
    5.  Debit
    6.  Credit Card
    7.  Gift Card
    8.  Store Card
    9.  Check From Office 
    10. Credit Memo    

# Synfony Console
    The synfony console is a component that provides a console for a user to select options from a screen.
    when calling the process with the command "translate" a list of commands will be presented to the user
    Console Commands are defined in the src\Commands directory.
    Commands are registered in the program entry point (root/translate) under add commands

# Enviornment Variables
    in the program entry point, enviornment variables are loaded from the .env file in the project root directory
    this lets the process have soft coded variables that may be different between environments such as local dev, qa server, production server

# Error handling
    A global error handler is created in the program entry point
    The error handler overrides the php error handler so that any errors seen can be emailed and logged
    the location of the error logs and who is emailed is defined in the .env environment file
    any reoccuring error is only logged so that emails do not flood the users email inbox. 
    if a reoccuring error exceeds a maximum threshold the process shuts itself down and notifies the users
    log and email classes also are used throughout the program for additional handling. 
    in various places the process will catch errors and send an email specific to that error using these classes

# Employee Import Process
    This process is called via one of the translate commands on the menu and handles creating XML for employees that is ultimately imported into the Sales Audit System.
    The Employee Move Application handles the creation of the XML and is driven off of an SQL View called "IMSLSMVW1" on the IBMi.
    This command is scheduled via Crontab and can be configured or disabled if necessary. It can also be manually called as well.
    The files ultimately end up on the two Sales Audit servers (LPSA or LPSATEST) under a folder called "CRDM Import". 
    If testing in a dev box or local environment, mappings will need to be confirmed they are set up correctly within the root Storage folder so that the program
    has access to the folder.

# Sales Translate Mountings
    In order for the translate to access the network drives where XML and other files are being created or updated, mountings are necessary in order for the process to find
    the files/folders being used. 

    Linux Commands to view Mountings: 
        cd /Storage
        ls -a

    LPPHP01 (QA)
        - AuditWorksQA
        - EmployeeImportPROD
        - InternetSalesPROD
        - SalesAuditQA
        - SalesExportPROD
        - SalesPostingPROD

    LPPHPPROD01 (QA)
        - AuditWorksPROD
        - EmployeeImportPROD
        - InternetSalesPROD
        - SalesAuditPROD
        - SalesExportPROD
        - SalesPostingPROD

# Creating a new Mounting
    1. Verify Credentials are correct with user and password within "/home/vagrant/.smbcredentials" if you're using a Dev Box, Different path but same location for QA/Production
        a. cat /home/vagrant/.smbcredentials
        b. sudo nano /home/vagrant/.smbcredentials
        c. Verify the credentials, Enter your username/password: 
            username=NEWUSER
            password=PASSWORD
            domain=LPDOMAIN
        d. There is an example .smbcredentials file within in the project, you can copy it to the appropriate location.
            cp .smbcredentials.example .smbcredentials
            Then make sure it is in the /home/vagrant folder
    2. Make folder directories within the root Storage folder
        mkdir <foldername>
    3. Once you have verified that credentials are correct and have created the folder directories within the /Storage folder, it is time to add the mountings to the /etc/fstab file.
        a. This file is what allows the linux box to mount that empty directory to the specific file location that is trying to be mapped to.
    4. There is an example file called: .fstab.example
        a. These are the current mappings for QA/PROD.
        b. You can add in new ones, but if you're looking to map to QA or Production Sales Audit Servers you will use these mappings.
    5. Once all files have been updated and verified that directories are created/credentials are correct/mappings are set, then you can run the mounting command.
        a. sudo mount -all
    6. You should not see any errors or if you happen to see "Directory is busy, etc." this is okay. 
    7. The directories now in /Storage should be highlighted and if you navigate it within them, you should see the directory and files you mapped to.


# Example Files
    1. .env.example
        a. This is a copy of ".env" which holds all environment variables. Create a copy of this called .env
        b. cp .env.example .env
    2. .fstab.example
        a. This is a copy of "/etc/fstab" which holds all mappings for directories to mounted. Used to access the Sales Audit Servers.
    3. .smbcredentials.example
        a. This is a copy of ".smbcredentials" which holds your credentials information such as your username and password. This needs to be updated with your current username/password.
        b. cp .smbcredentials.example .smbcredentials
        c. Move it to your home folder, "cd ~"
