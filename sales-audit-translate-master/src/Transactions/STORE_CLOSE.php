<?php

namespace Lamps\Transactions;

class STORE_CLOSE extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->rootTag = 'SYSCONTROL';

        $this->tags['ID'] = "SYSCTL.{$this->store}.{$this->date}.{$this->transactionId}.EOD";
        $this->tags['EOD'] = [];
    }
}