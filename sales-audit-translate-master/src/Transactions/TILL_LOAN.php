<?php

namespace Lamps\Transactions;

class TILL_LOAN extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->rootTag = 'POS';

        $this->tags['TillNo'] = $this->registerId;
        
        $amount = $data['TransactionData']['EnteredAmount'][0]['Amount'];

        $this->tags['TILL'] = [
            "Mgr" => $this->operator,
            "Amt" => $amount * -1,
            "IsPickup" => '1',
            "PickupWarnings" => '0',
            "IsVoid" => "1"
        ];
        
    }
}