<?php

namespace Lamps\Transactions;

class TILL_PAYOUT extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->rootTag = 'POS';
        $this->tags['Id'] = "POS.{$this->store}.{$this->date}.{$this->registerId}.{$this->transactionId}";
        $this->tags['TillNo'] = $this->registerId;
        
        $transactionHasComments = ! empty($data['TransactionData']['Comments']);
        $ammount = $data['TransactionData']['EnteredAmount'][0]['Amount'];
        
        $this->tags['TRADE'] = [
            "Amt" => $ammount,
            "IsPaidIO" => '1',
            "TENDER" => [
                    "Amt" => $ammount,
                    "Idx" => '1',
                    "IsCash" => '1'
                ],
            "PAIDIO" => [
                "Mgr" => ltrim($data['UpdatedBy'],'0'),
                "Idx" => ($transactionHasComments) ? '8' : '1',
                "AcctWkstn" => $this->registerId,
                "IsOut" => '1',
                "PaidOutEmployee" => ltrim($data['AssociateId'],'0')
            ]    
        ];
        
        if($data['TransactionData']['ReasonCode'] == "Contest SPIFF") $this->tags['TRADE']['PAIDIO']['Idx'] = '3';
        if($data['TransactionData']['ReasonCode'] == "Merchandise SPIFF") $this->tags['TRADE']['PAIDIO']['Idx'] = '13';
        
        if($transactionHasComments) {

            //transactions with comments have a gen info tag
            $this->tags['TRADE']['PAIDIO']['GENINFO'] = [
                "Name" => $data['TransactionData']['ReasonCode'],
                "FIELD" => [
                    "Data" => $data['TransactionData']['Comments'],
                    "Name" => "Comments"
                ]
            ];

        }

        //transactions without comments have a reason tag
        $this->tags['TRADE']['PAIDIO']['REASON'] = [
            "Code" => ltrim(substr($data['ReasonCode']['ReasonId'],4,4),'0')
        ];

    }
}