<?php

namespace Lamps\Transactions;

class BANK_DEPOSIT extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->rootTag = 'CASHOFF';
        $this->tags['Id'] = "CO.{$this->store}.25287.{$this->date}.PKUP";

        //Initialize Amount Values
        $cashAmount = 0;
        $checkAmount = 0;
        $travelerAmount = 0;

        //Get Actual Amounts for tenders
        foreach($data['TransactionData']['EnteredAmount'] as $actual)
        {
            switch ($actual['TenderTypeId'])
            {
                case "CASH":
                    $cashAmount = $actual['Amount'];
                    break;
                case "CHECK":
                    $checkAmount = $actual['Amount'];
                    break;
                case "TRAVELER'S CHECK":
                    $travelerAmount = $actual['Amount'];
                    break;
            }
        }

        $this->tags['SETTLE'] = [
            "IsFinal" => "1",
            "CashFloat" => $cashAmount + $checkAmount + $travelerAmount,
            "WkStn" => $data['RegisterID'],
            "Till" => $data['TillID'],
            "IsWkStn" => "1",
            "DateBus" => $this->createdAt,
            "IsTill" => "1",
            "Date" => $this->createdAt,
            "Comment" => "",
            "IsVoid" => "1",
            "ACTUAL" =>
            [
                "TENDER1" =>
                [
                    "Amt" => $cashAmount,
                    "Idx" => "1",
                    "Count" => "0"
                ],
                "TENDER2" =>
                [
                    "Amt" => $checkAmount,
                    "Idx" => "2",
                    "Count" => "0"
                ],
                "TENDER0" =>
                [
                    "Amt" => $travelerAmount,
                    "Idx" => "16",
                    "Count" => "0"
                ]
            ]
        ];
        
        // $this->tags['PICKUP'] = [
        //     "IsDeclare" => "1",
        //     "IsFinal" => "1",
        //     "Amt" => $amount,
        //     "Till" => $data['RegisterID'],
        //     "DateBus" => $this->createdAt,
        //     "AcctWkStn" => $data['RegisterID'],
        //     "Safe" => "888",
        //     "IsTill" => "1",
        //     "Date" => $this->createdAt,
        //     "Comment" => "",
        //     "TENDER1" =>
        //     [
        //         "Amt" => "0",
        //         "Idx" => "1",
        //         "Count" => "0"
        //     ],
        //     "TENDER2" =>
        //     [
        //         "Amt" => "0",
        //         "Idx" => "2",
        //         "Count" => "0"
        //     ]
        // ];
        
    }
}