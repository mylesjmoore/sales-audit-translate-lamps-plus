<?php

namespace Lamps\Transactions;
use Lamps\Database\Query;
use Lamps\Database\Connection;

class TILL_RECONCILE extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);
        $this->query = new Query(Connection::make()); 
        $this->rootTag = 'CASHOFF';
        $this->tags['Id'] = "CO.{$this->store}.25287.{$this->date}.SETTLE";

        //Set the register to be the same as the till
        $this->tags['WkStn'] = $data['TillID'];

        //Get the Pickup/Loan amounts for this register for the Settle(Till Reconcile)
        $tillPickup = 0;
        $tillLoan = 0;
        $settleData = $this->query->getSettleAmounts($this->store,substr($this->createdAt,0,10),$data['TillID'],$table);
        if($settleData)
        {
            foreach($settleData as $settleAmount)
            {
                switch (trim($settleAmount['TENDERTYPE']))
                {
                    case "TILL_PICKUP":
                        $tillPickup = $settleAmount['AMOUNT'];
                        break;
                    case "TILL_LOAN":
                        $tillLoan = $settleAmount['AMOUNT'] * -1;
                        break;
                }
            }
        }
        
        //Get the total for all tenders on this register for the Pickup(Bank Deposit)
        //Cash/Check
        $cashTotal = $tillPickup + $tillLoan;
        $checkTotal = 0;
        $checkCount = 0;
        $tillAmount = 0;

        //Tender Counts
        $visaCount = 0;
        $amexCount = 0;
        $mastercardCount = 0;
        $discoverCount = 0;
        $debitCount = 0;
        $arCount = 0;
        $giftCardCount = 0;
        $creditMemoCount = 0;
        $storeCardCount = 0;
        $cfoCount = 0;

        //Tender Amounts
        $visaAmount = 0;
        $amexAmount = 0;
        $mastercardAmount = 0;
        $discoverAmount = 0;
        $debitAmount = 0;
        $arAmount = 0;
        $giftCardAmount = 0;
        $creditMemoAmount = 0;
        $storeCardAmount = 0;
        $cfoAmount = 0;
        
        $pickupData = $this->query->getPickupAmounts($this->store,substr($this->createdAt,0,10),$data['TillID'],$table);
        if($pickupData)
        {
            foreach($pickupData as $pickupAmount)
            {
                switch (trim($pickupAmount['TENDERTYPE']))
                {
                    case "Check":
                        $checkTotal = $pickupAmount['AMOUNT'];
                        $checkCount = $pickupAmount['COUNT'];
                        break;
                    case "AR":
                        $arAmount = $pickupAmount['AMOUNT'];
                        $arCount = $pickupAmount['COUNT'];
                        break;
                    case "Debit":
                        $debitAmount = $pickupAmount['AMOUNT'];
                        $debitCount = $pickupAmount['COUNT'];
                        break;
                    case "Credit Memo":
                        $creditMemoAmount = $pickupAmount['AMOUNT'];
                        $creditMemoCount = $pickupAmount['COUNT'];
                        break;
                    case "CFO":
                        $cfoAmount = $pickupAmount['AMOUNT'];
                        $cfoCount = $pickupAmount['COUNT'];
                        break;
                    case "Gift Card":
                        $giftCardAmount = $pickupAmount['AMOUNT'];
                        $giftCardCount = $pickupAmount['COUNT'];
                        break;
                    case "Store Card":
                        $storeCardAmount = $pickupAmount['AMOUNT'];
                        $storeCardCount = $pickupAmount['COUNT'];
                        break;
                    case "Credit Card":
                        switch (trim($pickupAmount['CARDTYPE']))
                        {
                            case "Visa":
                                $visaAmount = $pickupAmount['AMOUNT'];
                                $visaCount = $pickupAmount['COUNT'];
                                break;
                            case "Amex":
                                $amexAmount = $pickupAmount['AMOUNT'];
                                $amexCount = $pickupAmount['COUNT'];
                                break;
                            case "MasterCard":
                                $mastercardAmount = $pickupAmount['AMOUNT'];
                                $mastercardCount = $pickupAmount['COUNT'];
                                break;
                            case "Discover":
                                $discoverAmount = $pickupAmount['AMOUNT'];
                                $discoverCount = $pickupAmount['COUNT'];
                                break;
                        }
                }
            }
        }

        //Get the total for Pickup(Bank Deposit)
        $tillAmount = $tillAmount + $cashTotal;
        

        //Get Actual Amounts for tenders
        $travelerActual = 0.00;
        $travelerActualCount = 0;
        $checkActual = 0.00;
        $checkActualCount = 0;
        foreach($data['TransactionData']['EnteredAmount'] as $actual)
        {
            switch ($actual['TenderTypeId'])
            {
                case "CASH":
                    $cashActual = $actual['Amount'];
                    break;
                case "TRAVELER'S CHECK":
                    $travelerActual = $actual['Amount'];
                    $travelerActualCount = $actual['Count'];
                    break;
                case "CHECK":
                    $checkActual = $actual['Amount'];
                    $checkActualCount = $actual['Count'];
                    break;
            }
        }

        //Get Expected Amounts for tenders
        $travelerExpected = 0.00;
        $travelerExpectedCount = 0;
        $checkExpected = 0.00;
        $checkExpectedCount = 0;
        foreach($data['TransactionData']['ExpectedAmount'] as $expected)
        {
            switch ($expected['TenderTypeId'])
            {
                case "CASH":
                    $cashExpected = $expected['Amount'];
                    break;
                case "TRAVELER'S CHECK":
                    $travelerExpected = $expected['Amount'];
                    $travelerExpectedCount = $expected['Count'];
                    break;
                case "CHECK":
                    $checkExpected = $expected['Amount'];
                    $checkExpectedCount = $expected['Count'];
                    break;
            }
        }
        
        $this->tags['SETTLE'] = [
            "IsFinal" => "1",
            "CashFloat" => $data['TransactionData']['EnteredFloat']['0']['Amount'],
            "WkStn" => $data['RegisterID'],
            "Till" => $data['TillID'],
            "IsWkStn" => "1",
            "DateBus" => $this->createdAt,
            "IsTill" => "1",
            "Date" => $this->createdAt,
            "Comment" => "",
            "IsVoid" => "1",
            "ACTUAL" =>
            [
                "TENDER1" =>
                [
                    "Amt" => $tillPickup + $tillLoan,
                    "Idx" => "1",
                    "Count" => "0"
                ],
                "TENDER2" =>
                [
                    "Amt" => $checkActual,
                    "Idx" => "2",
                    "Count" => "0"
                ],
                "TENDER0" =>
                [
                    "Amt" => $travelerActual,
                    "Idx" => "16",
                    "Count" => "0"
                ],
                // "TENDER4" =>
                // [
                //     "Amt" => $visaAmount,
                //     "Idx" => "7",
                //     "Count" => "0"
                // ],
                // "TENDER5" =>
                // [
                //     "Amt" => $mastercardAmount,
                //     "Idx" => "8",
                //     "Count" => "0"
                // ],
                // "TENDER6" =>
                // [
                //     "Amt" => $amexAmount,
                //     "Idx" => "9",
                //     "Count" => "0"
                // ],
                // "TENDER7" =>
                // [
                //     "Amt" => $discoverAmount,
                //     "Idx" => "10",
                //     "Count" => "0"
                // ],
                // "TENDER8" =>
                // [
                //     "Amt" => $debitAmount,
                //     "Idx" => "3",
                //     "Count" => "0"
                // ],
                "TENDER9" =>
                [
                    "Amt" => $giftCardAmount,
                    "Idx" => "4",
                    "Count" => "0"
                ],
                "TENDER10" =>
                [
                    "Amt" => $creditMemoAmount,
                    "Idx" => "11",
                    "Count" => "0"
                ],
                "TENDER11" =>
                [
                    "Amt" => $storeCardAmount,
                    "Idx" => "26",
                    "Count" => "0"
                ],
                "TENDER12" =>
                [
                    "Amt" => $arAmount,
                    "Idx" => "29",
                    "Count" => "0"
                ],
                "TENDER13" =>
                [
                    "Amt" => $cfoAmount,
                    "Idx" => "15",
                    "Count" => "0"
                ]
            ],
            "EXPECTED" =>
            [
                "TENDER1" =>
                [
                    "Amt" => $cashExpected + $tillPickup + $tillLoan,
                    "Idx" => "1",
                    "Count" => "0"
                ],
                "TENDER2" =>
                [
                    "Amt" => $checkExpected,
                    "Idx" => "2",
                    "Count" => "0"
                ],
                "TENDER0" =>
                [
                    "Amt" => $travelerExpected,
                    "Idx" => "16",
                    "Count" => "0"
                ],
                // "TENDER4" =>
                // [
                //     "Amt" => $visaAmount,
                //     "Idx" => "7",
                //     "Count" => "0"
                // ],
                // "TENDER5" =>
                // [
                //     "Amt" => $mastercardAmount,
                //     "Idx" => "8",
                //     "Count" => "0"
                // ],
                // "TENDER6" =>
                // [
                //     "Amt" => $amexAmount,
                //     "Idx" => "9",
                //     "Count" => "0"
                // ],
                // "TENDER7" =>
                // [
                //     "Amt" => $discoverAmount,
                //     "Idx" => "10",
                //     "Count" => "0"
                // ],
                // "TENDER8" =>
                // [
                //     "Amt" => $debitAmount,
                //     "Idx" => "3",
                //     "Count" => "0"
                // ],
                "TENDER9" =>
                [
                    "Amt" => $giftCardAmount,
                    "Idx" => "4",
                    "Count" => "0"
                ],
                "TENDER10" =>
                [
                    "Amt" => $creditMemoAmount,
                    "Idx" => "11",
                    "Count" => "0"
                ],
                "TENDER11" =>
                [
                    "Amt" => $storeCardAmount,
                    "Idx" => "26",
                    "Count" => "0"
                ],
                "TENDER12" =>
                [
                    "Amt" => $arAmount,
                    "Idx" => "29",
                    "Count" => "0"
                ],
                "TENDER13" =>
                [
                    "Amt" => $cfoAmount,
                    "Idx" => "15",
                    "Count" => "0"
                ]
            ]
        ];
        
        $this->tags['PICKUP'] = [
            "IsDeclare" => "1",
            "IsFinal" => "1",
            "Amt" => $tillAmount,
            "Till" => $data['TillID'],
            "DateBus" => $this->createdAt,
            "AcctWkStn" => $data['RegisterID'],
            "Safe" => "888",
            "IsTill" => "1",
            "Date" => $this->createdAt,
            "Comment" => "",
            // "TENDER1" =>
            // [
            //     "Amt" => $cashTotal + $data['TransactionData']['EnteredFloat']['0']['Amount'],
            //     "Idx" => "1",
            //     "Count" => "0"
            // ],
            "TENDER2" =>
            [
                "Amt" => $checkActual,
                "Idx" => "2",
                "Count" => $checkActualCount
            ],
            "TENDER0" =>
            [
                "Amt" => $travelerActual,
                "Idx" => "16",
                "Count" => $travelerActualCount
            ],
            // "TENDER3" =>
            // [
            //     "Amt" => $debitAmount,
            //     "Idx" => "3",
            //     "Count" => $debitCount
            // ],
            // "TENDER4" =>
            // [
            //     "Amt" => $visaAmount,
            //     "Idx" => "7",
            //     "Count" => $visaCount
            // ],
            // "TENDER5" =>
            // [
            //     "Amt" => $mastercardAmount,
            //     "Idx" => "8",
            //     "Count" => $mastercardCount
            // ],
            // "TENDER6" =>
            // [
            //     "Amt" => $amexAmount,
            //     "Idx" => "9",
            //     "Count" => $amexCount
            // ],
            // "TENDER7" =>
            // [
            //     "Amt" => $discoverAmount,
            //     "Idx" => "10",
            //     "Count" => $discoverCount
            // ],
            "TENDER9" =>
            [
                "Amt" => $giftCardAmount,
                "Idx" => "4",
                "Count" => $giftCardCount
            ],
            "TENDER10" =>
            [
                "Amt" => $creditMemoAmount,
                "Idx" => "11",
                "Count" => $creditMemoCount
            ],
            "TENDER11" =>
            [
                "Amt" => $storeCardAmount,
                "Idx" => "26",
                "Count" => $storeCardCount
            ],
            "TENDER12" =>
            [
                "Amt" => $arAmount,
                "Idx" => "29",
                "Count" => $arCount
            ],
            "TENDER13" =>
            [
                "Amt" => $cfoAmount,
                "Idx" => "15",
                "Count" => $cfoCount
            ]
        ];
        
    }
}