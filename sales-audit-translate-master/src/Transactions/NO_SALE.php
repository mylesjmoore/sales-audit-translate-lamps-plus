<?php

namespace Lamps\Transactions;

class NO_SALE extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->rootTag = 'POS';
        $this->tags['Id'] = "POS.{$this->store}.{$this->date}.{$this->registerId}.{$this->transactionId}";
        $this->tags['TillNo'] = $this->registerId;

        $this->tags['TRADE'] = [
            "IsSale" => "1",
            "Amt" => "0",
            "IsAbort" => "1"   
        ];
    }
}