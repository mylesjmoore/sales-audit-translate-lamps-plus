<?php

namespace Lamps\Transactions;
use Lamps\Database\Query;
use Lamps\Database\Connection;

class CASH_MANAGEMENT_SUMMARY extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->query = new Query(Connection::make()); 
        $this->rootTag = 'CASHOFF';
        $transactionNumber = "90" . $this->store;
        $this->tags['Tran'] = $transactionNumber;
        $this->tags['Id'] = "POS.{$this->store}.{$this->date}.{$this->registerId}.{$transactionNumber}";
        $this->tags['TillNo'] = $this->registerId;

        //Get the Pickup/Loan amounts for this register for the Settle(Till Reconcile)
        $tillPickup = 0;
        $tillLoan = 0;
        $settleData = $this->query->getCashSummaryAmounts($this->store,substr($this->createdAt,0,10),$table);
        if($settleData)
        {
            foreach($settleData as $settleAmount)
            {
                switch (trim($settleAmount['TENDERTYPE']))
                {
                    case "TILL_PICKUP":
                        $tillPickup = $tillPickup + $settleAmount['AMOUNT'];
                        break;
                    case "TILL_LOAN":
                        $tillLoan = $settleAmount['AMOUNT'] * -1;
                        break;
                }
            }
        }
        
        //Get the total for all tenders on this register for the Pickup(Bank Deposit)
        //Tender Amounts
        //Cash/Check
        $cashTotal = $tillPickup + $tillLoan;
        $checkTotal = 0;
        $arAmount = 0;
        $giftCardAmount = 0;
        $creditMemoAmount = 0;
        $storeCardAmount = 0;
        $cfoAmount = 0;
        
        $pickupData = $this->query->getTotalPickupAmounts($this->store,substr($this->createdAt,0,10),$table);
        if($pickupData)
        {
            foreach($pickupData as $pickupAmount)
            {
                switch (trim($pickupAmount['TENDERTYPE']))
                {
                    case "Check":
                        $checkTotal = $checkTotal + $pickupAmount['AMOUNT'];
                        break;
                    case "AR":
                        $arAmount = $arAmount + $pickupAmount['AMOUNT'];
                        break;
                    case "Credit Memo":
                        $creditMemoAmount = $creditMemoAmount + $pickupAmount['AMOUNT'];
                        break;
                    case "CFO":
                        $cfoAmount = $cfoAmount + $pickupAmount['AMOUNT'];
                        break;
                    case "Gift Card":
                        $giftCardAmount = $giftCardAmount + $pickupAmount['AMOUNT'];
                        break;
                    case "Store Card":
                        $storeCardAmount = $storeCardAmount + $pickupAmount['AMOUNT'];
                        break;
                }
            }
        }
        
        $this->tags['SETTLE'] = [
            "IsFinal" => "1",
            //"CashFloat" => "200",
            "WkStn" => $this->registerId,
            "Till" => $this->registerId,
            "IsWkStn" => "1",
            "DateBus" => $this->createdAt,
            "IsTill" => "1",
            "Date" => $this->createdAt,
            "Comment" => "",
            //"IsVoid" => "1",
            "ACTUAL" =>
            [
                "TENDER1" =>
                [
                    "Amt" => $cashTotal,
                    "Idx" => "1",
                    "Count" => "0"
                ],
                "TENDER2" =>
                [
                    "Amt" => $checkTotal,
                    "Idx" => "2",
                    "Count" => "0"
                ],
                "TENDER9" =>
                [
                    "Amt" => $giftCardAmount,
                    "Idx" => "4",
                    "Count" => "0"
                ],
                "TENDER10" =>
                [
                    "Amt" => $creditMemoAmount,
                    "Idx" => "11",
                    "Count" => "0"
                ],
                "TENDER11" =>
                [
                    "Amt" => $storeCardAmount,
                    "Idx" => "26",
                    "Count" => "0"
                ],
                "TENDER12" =>
                [
                    "Amt" => $arAmount,
                    "Idx" => "29",
                    "Count" => "0"
                ],
                "TENDER13" =>
                [
                    "Amt" => $cfoAmount,
                    "Idx" => "15",
                    "Count" => "0"
                ]
            ]
        ];




    }
}