<?php

namespace Lamps\Transactions;

class TILL_CLOSE extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->rootTag = 'POS';

        $this->tags['TillNo'] = $this->registerId;
        
        $this->tags['TILL'] = [
            "IsLogOff" => '1',
            "Mgr" => $this->operator
        ];
    }
}