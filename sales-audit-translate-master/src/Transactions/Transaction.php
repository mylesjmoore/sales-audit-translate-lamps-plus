<?php

namespace Lamps\Transactions;

use Carbon\Carbon;

class Transaction
{
    public $rootTag;
    protected $store;
    protected $operator;
    protected $registerId;
    protected $date;
    protected $createdAt;
    protected $transactionId;
    public $tags = [];

    protected $centralStores = ['44', '47', '63'];
    protected $mountainStores = ['28', '30'];
    protected $mountainStoresNoDST = ['38', '42', '60'];

    public function __construct($data)
    {
        $timestamp = (new Carbon($data['CreatedTimestamp'], 'GMT'));
        $this->date = $this->localizeTimestamp($timestamp, 'Ymd');
        $this->createdAt = $this->localizeTimestamp($timestamp, 'Y-m-d\\TH:i:s');
    }

    protected function localizeTimestamp($time, $format)
    {
        if (in_array($this->store, $this->centralStores)) return $time->tz('America/Chicago')->format($format);
        if (in_array($this->store, $this->mountainStores)) return $time->tz('America/Denver')->format($format);
        if (in_array($this->store, $this->mountainStoresNoDST)) return $time->tz('America/Phoenix')->format($format);

        return $time->tz('America/Los_Angeles')->format($format);

    }

}