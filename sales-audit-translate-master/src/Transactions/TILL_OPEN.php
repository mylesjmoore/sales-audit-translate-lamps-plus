<?php

namespace Lamps\Transactions;

class TILL_OPEN extends BackOfficeTransaction
{
    public function __construct($data,$table)
    {
        parent::__construct($data,$table);

        $this->rootTag = 'CASHOFF';
        $this->tags['Id'] = "CO.{$this->store}.25287.{$this->date}.SETTLE";
        
        $this->tags['SETTLE'] = [
            "CashFloat" => $data['BoTransactionDetail']['0']['Amount'],
            "WkStn" => $data['RegisterID'],
            "Till" => $data['TillID'],
            "IsWkStn" => "1",
            "DateBus" => $this->createdAt,
            "IsTill" => "1",
            "Date" => $this->createdAt,
            "IsVoid" => "1",
            "ACTUAL" =>
            [
                "TENDER" =>
                [
                    "Amt" => $data['TransactionData']['EnteredFloat']['0']['Amount'],
                    "Idx" => "1",
                    "Count" => "0"
                ]
            ],
            "EXPECTED" =>
            [
                "TENDER1" =>
                [
                    "Amt" => $data['TransactionData']['ExpectedFloat']['0']['Amount'],
                    "Idx" => "1",
                    "Count" => "0"
                ],
                "TENDER2" =>
                [
                    "Amt" => "0",
                    "Idx" => "2",
                    "Count" => "0"
                ]
            ]
        ];

    }
}