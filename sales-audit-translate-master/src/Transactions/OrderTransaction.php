<?php

namespace Lamps\Transactions;
use Lamps\Database\Query;
use Lamps\Database\Connection;
use \DateTime;
use Carbon\Carbon;

class OrderTransaction extends Transaction
{
    protected $query;

    public function __construct($data,$table,$recordId)
    {
        
        Parent::__construct($data,$table,$recordId);

        $this->rootTag = 'POS';
        $transactionId = ltrim(substr($data['OrderId'],15,4),'0');
        $store = ltrim(substr($data['OrderId'],8,4),'0');
        $this->operator = ltrim($data['OrderCaptureDetail']['0']['OperatorId'], '0');
        $registerId = ltrim($data['OrderCaptureDetail']['0']['RegisterId'],'0');
        $tillNumber = (isset($data['OrderCaptureDetail']['0']['TillId'])) ?
            ltrim($data['OrderCaptureDetail']['0']['TillId']) : $registerId;
        $businessDate = str_replace('-', '', $data['BusinessDate']);

        $this->tags = [
            "Op" => $this->operator,
            "WkStn" => $registerId,
            "TillNo" => $tillNumber,
            "Tran" => $transactionId,
            "ID" => "POS.{$store}.{$businessDate}.{$registerId}.{$transactionId}",
            "Store" => $store,
            "Time" => $this->createdAt,
            "OrderId" => $data['OrderId'],
            "TRADE" => $this->tradeTags($data)
        ];

        //Validate Array
        $this->query = new Query(Connection::make()); 
        $this->tags = $this->validateArray($this->tags,$data,$table,$recordId);

    }

    private function validateArray($xmlArray,$data,$table,$recordId)
    {
        $errorMessage = null;
        $validTransaction = array(01);
        
        //Transaction ID Check
        if (! $this->tags['OrderId'])
        {
            $errorMessage = "Missing Transaction ID."; 
            $xmlArray = null;
        }
        //Valid Transaction ID 
        elseif (!in_array(substr($data['OrderId'],0,2), $validTransaction))
        {
            $errorMessage = "Transaction sent as void:{$this->tags['OrderId']}";
            if(substr($this->tags['OrderId'],0,2) == 07) $errorMessage = "Tender Exchange:{$this->tags['OrderId']}";
            
        }
        //Tender Information Check
        elseif (! $data['IsCancelled']) 
        {
            //Check for Invoice Information
            // if(empty($data['Invoice']))
            // {
            //     $errorMessage = "No Invoice Information. ";
            //     $xmlArray = null;
            // }
            
            //Check for Tax Information
            // if(is_null($data['TotalTaxes']))
            // {
            //     $errorMessage = "Missing Tax Amount. Check JSON for *TotalTaxes*";
            //     $xmlArray = null;
            // }
            
            //Check for Tender Information
            if(! array_key_exists("TENDER0",$this->tags['TRADE']))
            {
                //Check if this is an even exchange, which means the total amount should be 0
                if($this->tags['TRADE']['Amt'] > 0)
                {
                    $errorMessage = "Missing Tender Information. Check JSON Payments.";
                    $xmlArray = null;
                }
            } 
        }
        
        if(! is_null($errorMessage)) $this->query->setNotProcessedErrorMessage($table,$recordId,$this->tags['OrderId'],$errorMessage);

        return $xmlArray;
    }
     
    private function tradeTags($data)
    {
        $tradeTags["IsTenderExchng"] = null;

        $tradeTags["EMPLOYEE"] = 
        [
            "NumStr" => null
        ];
        
        if($data['IsCancelled'])     
        {
            return [
                "IsSale" => "1",
                "Amt" => "0",
                "IsAbort" => "1"
            ];
        }

        //Void Gift Card Inquiries and Unknown Transactions
        $validTransactions = array(01,07);
        if(!in_array(substr($data['OrderId'],0,2), $validTransactions))
        {
            return [
                "IsSale" => "1",
                "Amt" => "0",
                "IsAbort" => "1"
            ];
        }


        //Check for Payment Information
        $hasPaymentInformation = ! empty($data['Payment']['0']['PaymentMethod']);
        $payments = ($hasPaymentInformation) ? $data['Payment']['0']['PaymentMethod'] : null;

        //TODO: Check for Employee # for an Employee Discount

        $transactionHasItems = count($data['OrderLine']) > 0;
        if ($transactionHasItems) $tradeTags["IsSale"] = "1";

        $tradeTags["Amt"] = $data['OrderTotal'];

        $totalTaxes = $data['TotalTaxes'];
        $tradeTags["IsTax"] = ($totalTaxes) ? "1" : null;

        //Validate Customer ID and Loyalty Number
        if(!isset($data['CustomerId']) or !is_numeric($data['CustomerId']) or empty($data['CustomerId'])) $data['CustomerId'] = null;
        if(!isset($data['LoyaltyNumber']) or !is_numeric($data['LoyaltyNumber']) or empty($data['LoyaltyNumber'])) $data['LoyaltyNumber'] = null;
        $customerInfo = $this->customerInfo($data);

        if (isset($data['CustomerId']) or isset($data['LoyaltyNumber'])) {
            $tradeTags['CUSTOMER'] = $this->customerTag($customerInfo);
            $tradeTags['GENINFO(Rewards)'] = $this->rewardsTag($customerInfo['RewardsNumber']);
        }

        if ($this->hasArPayment($payments)) {
            $tradeTags['GENINFO(Customer)'] = $this->arCheck($payments,$customerInfo['RewardsNumber'],"Customer");
            $tradeTags['GENINFO(Purchaser)'] = $this->arCheck($payments,$customerInfo['RewardsNumber'],"Purchaser");
        }

        if ($transactionHasItems) $tradeTags = $this->itemTags($data['OrderLine'],$tradeTags,$totalTaxes);

        $tradeTags['TAX'] = $this->taxTags($totalTaxes,$data['IsTaxExempt'],$data['TaxExemptId']);

        if($hasPaymentInformation) $tradeTags = $this->tenderTags($data,$tradeTags,$payments,$customerInfo);

        //Check for Gift Cards, Get Gift Card Number (Gift Card Number is only located in the Tender Information)
        $count = 1;
        $giftCards = [];
        foreach($tradeTags as $giftCard => $nodes)
        {
            if(substr($giftCard,0,14) == "TENDERGIFTCARD")
            {
                $giftCards["GiftCard{$count}"] = $nodes['Acct'];
                $count++;
                unset($tradeTags[$giftCard]);
            }
            
        }

        //If there is a gift card, add the gift card number
        if(! is_null($giftCards))
        {
            $count = 1;
            foreach($tradeTags as $giftCard => $nodes)
            {
                if(substr($giftCard,0,4) == "ITEM" and $nodes['Idx'] == "1")
                {
                    $tradeTags[$giftCard]['Acct'] = (array_key_exists("GiftCard{$count}", $giftCards)) ? $giftCards["GiftCard{$count}"] : null;
                    $count++;
                }
                
            }
        }

        //For Tender Exchange, add in flag, amt tag, and remove any tax tags
        if(substr($data['OrderId'],0,2) == 07) 
        {
            $tradeTags["IsTenderExchng"] = "1"; 
            $tradeTags['Amt'] = $tradeTags['TENDER0']['Amt'];
            $tradeTags['TAX'] = null;
            $tradeTags['TENDEREXCHNG']  =
            [
                "Mgr" => ltrim($data['OrderCaptureDetail']['0']['OperatorId'], '0')
            ]; 
        }

        return $tradeTags;
    }

    private function taxTags($taxAmount, $isTaxExempt, $taxExemptId)
    {
        if($isTaxExempt) return [
            "ExNum" => $taxExemptId,
            "RoundedAmount" => $taxAmount,
            "Amt" =>  $taxAmount,
            "IsOverrideZone" => "1",
            "IsExempt" => "1",
            "IsTranLevel" => "1"
        ];

        return [
            "RoundedAmount" => $taxAmount,
            "Amt" =>  $taxAmount,
            "IsRegular" => "1",
        ];
    }

    private function rewardsTag($rewardsNumber)
    {
        return [
            "Name" =>  "rewardmem",
            "FIELD" => [
                "Data" => $rewardsNumber,
                "Name" => "Rewards_No",
            ]
        ];
    }

    private function itemTags($items,$tradeTags,$tax)
    {
        foreach($items as $index => $item)
        {
            $quantity = ltrim($item['Quantity'],'0');
            $price = ltrim($item['UnitPrice'],'0');
            $sku = ltrim($item['ItemId']);

            //if voided, dont generate item tag
            if(! $quantity) continue;

            //Check if there is a Price Modification, Create the tag *Do not create for Gift Cards*
            $hadPriceMod = $item['IsPriceOverridden'];
            if($item['IsNonMerchandise']) $hadPriceMod = false;
            $notCompPrice = true;
            if($item['IsGiftCard']) $hadPriceMod = false;
            if($hadPriceMod) list($compPrice, $priceMod) = $this->priceMod($item);
            if($hadPriceMod) $notCompPrice = is_null($compPrice);
            
            //Create Class
            $Class = $item['ProductClass'];
            $Department = $item['ItemDepartmentNumber'];
            if(is_numeric($Class))
            {
                $ProductClass = $Department . $Class;
            }
            else
            {
                $ProductClass = $Department . 0 . 1;
            }

            //Check UPC
            if(is_numeric(ltrim($item['ItemBarcode'],'0')))
            {
                $upc = ltrim($item['ItemBarcode'],'0');
            }
            else
            {
                $upc = null;
            }

            //if transaction discounted, add either single or multi discount tags - TODO: check multi item, one discounted one not (does discount get var get reset every iteration)
            $isDiscounted = ! empty($item['OrderLineChargeDetail']);
            $discountInfo = $item['OrderLineChargeDetail'];
            $hasMultiDiscount = count($discountInfo) > 1;
            $discount = null;
            $IsEmployeeDiscount = false;
            if($isDiscounted) list($discount,$IsEmployeeDiscount) = ($hasMultiDiscount) ? $this->multiDiscountTag($discountInfo) : $this->singleDiscountTag($discountInfo[0]);
            
            //Check if discount was 0, which is being ignored in the discount tag logic, set to null if empty
            if(empty($discount)) $isDiscounted = null;
            
            //if item returned, add return tag 
            $isReturned = $item['IsReturn'];
            $return = null;
            $return = ($isReturned) ? $this->returnTag($item) : [];

            //For Giftcards, set flags
            $IsNonMdse = ($item['IsGiftCard']) ? "1" : null;

            //Remove class for Gift Cards
            if($item['IsGiftCard']) $ProductClass = null;

            //Check if it is Merchandise or Non-Merchandise
            $IsMdse = ($IsNonMdse) ? null : "1";

            //If this is a comp price, then set the discounted flag
            if($isDiscounted == false and $notCompPrice == false) $isDiscounted = "1";

            //If there is a price mod, use the original unit price
            if($hadPriceMod) $price = ltrim($item['OriginalUnitPrice'],'0');
            
            //Build Item Tag
            $tradeTags["ITEM{$index}"] =  [
                "Qty" => $quantity,
                "Acct" => null,
                //"Upc" => $upc,
                "IsMdse" => $IsMdse,
                "Class" => $ProductClass,
                "Price" => $price ?: 0,
                "OrigPrice" => ltrim($item['OriginalUnitPrice'],'0') ?: 0,
                "IsDiscount" => ($isDiscounted) ? "1" : null,
                "Idx" => ($item['IsGiftCard']) ? "1" : null,
                "Sku" => "01" . $sku,
                "IsValueCard" => ($item['IsGiftCard']) ? "1" : null,
                "IsNonMdse" => $IsNonMdse,
                "IsReturn" => ($isReturned) ? "1" : null,
                "IsKeyed" => "1",
                "Div" => ($item['IsGiftCard']) ? "1" : null,
                "IsTax" => ($tax) ? '1' : null,
                "IsPriceMod" => ($hadPriceMod) ? ($notCompPrice) ? '1' : null : null,
                "COMPPRICE" => ($hadPriceMod) ? $compPrice : null,
                "PRICEMOD" => ($hadPriceMod) ? $priceMod : null,
                "SALESPERSON" =>  [
                    "Op" => empty($item['OrderLineSalesAssociate']) ? $item['CreatedBy'] : ltrim($item['OrderLineSalesAssociate']['0']['AssociateId'],'0')
                ] 
            ]; 

            
            
            if(!is_numeric($tradeTags["ITEM{$index}"]['SALESPERSON']['Op'])) $tradeTags["ITEM{$index}"]['SALESPERSON']['Op'] = '0000';
            if(isset($discount)) $tradeTags["ITEM{$index}"] = array_merge($tradeTags["ITEM{$index}"],$discount);
            if(isset($return)) $tradeTags["ITEM{$index}"] = array_merge($tradeTags["ITEM{$index}"],$return);
            if($IsEmployeeDiscount) $tradeTags["EMPLOYEE"]["NumStr"] = $tradeTags["ITEM{$index}"]['SALESPERSON']['Op'];
            
        }

        return $tradeTags;
    }

    private function priceMod($item)
    {
        $compPrice = null;
        $priceMod = null;
        $PriceModReason = $item['OrderLinePriceOverrideHistory']['0']['Reason']['ReasonId'];
        $code = ltrim(substr($PriceModReason,4,4),0);
        if($PriceModReason == 'Price Match' or $PriceModReason == null) $code = '11';
        
        foreach($item['OrderLinePriceOverrideHistory'] as $priceModInfo)
        {
            //This is a Price Mod
            $priceMod = [
                "Price" => $priceModInfo['UpdatedPrice'],
                "REASON" => 
                [
                    "Code" => $code
                ],
                "GENINFO" => 
                [
                    "FIELD" => [
                        "Data" => $priceModInfo['Comments'],
                        "Name" => "Competitor"
                    ]
                ]
            ];
        }

        return array($compPrice, $priceMod);
    }

    private function customerTag($customerInfo)
    {
        return [
            "NumStr" => $customerInfo['RewardsNumber'],
            "Name" =>  substr($customerInfo['FullName'],0,20),
            "CUSTPRSN" =>  [
                "LName" => $customerInfo['LastName'],
                "FName" => substr($customerInfo['FirstName'],0,20),
            ],
            "ADDRESS" => [
                "HomePhone" => $customerInfo['PhoneNumber'],
                "EMail" => $customerInfo['Email'],
                "City" => null,
                "Country" => null,
                "State" => null,
                "Addr1" => null,
                "PostalCode" => null
            ]
        ]; 
    }

    private function returnTag($item)
    {
        $hasValidReceipt = ! empty($item['ParentOrderId']);
        
        return [
            "RETURN" => [
                "OrigTran" => ltrim(substr($item['ParentOrderId'],15,4),'0'),
                "OrigLineItemNum" => ltrim($item['ParentOrderLineId'],'0'),
                "HasReceipt" => ($hasValidReceipt) ? '1' : null,
                "HasNoReceipt" => (!$hasValidReceipt) ? '1' : null,
                "OrigWkStn" => ltrim(substr($item['ParentOrderId'],12,3),'0'),
                "OrigTime" => $this->localizeTimestamp((new Carbon($item['ParentLineCreatedTimestamp'], 'GMT')), 'Y-m-d'),
                "IsKeyed" => "1",
                "OrigStore" => ltrim(substr($item['ParentOrderId'],8,4),'0'),
                "REASON" => [
                    "Desc" => $item['OrderLineAdditional']['ReturnReason'],
                    "Code" => ($item['OrderLineAdditional']['ReturnReason'] == "Changed Mind - Return to Stock") ? "1" : "4"
                ]
            ]
        ];
        
    }

    

    private function singleDiscountTag($discountInfo)
    {
        //Discount Codes
        //  1 = Generic Item Percent Discount
        //  2 = Generic Item Dollar Discount
        //  3 = Coupon Percent Discount - item
        //  4 = Coupon Dollar Discount - item
        //  5 = Re-stocking Fee
        //  6 = Employee Discount
        //  7 = Generic Transaction Percent Discount
        //  8 = Generic Transaction Dollar Discount
        //  9 = Coupon Percent Discount - transaction level
        // 10 = Coupon Dollar Discount - transaction level
        // 11 = NOT USED
        // 12 = Merchandise Discount Card
        // 13 = Coupon Discount ('QP' type, validated) - item level
        // 14 = Coupon Dollar Discount ('QP' type, validated) - tran. level
        // 15 = Coupon Percent Discount ('QP' type, validated) - tran. level
        // 16 = Paper Merchandise Discount
        // 17 = Free CFL Bulb
        // 18 = Rewards Customer Discount on discountinued items

        //Get the discount values
        $this->DiscountType = substr($discountInfo['ChargeDisplayName'],0,11);
        $IsPercentageDiscount = ltrim($discountInfo['ChargePercent'],'0');
        $this->Reduction = ltrim($discountInfo['ChargeTotal'],'0');

        //Reward Values
        $Rewards = substr($discountInfo['ChargeDisplayName'],0,6);
        $IsRewardsDiscount = false;
        if($Rewards == 'Reward') $IsRewardsDiscount = true;

        //Coupon Values
        $IsCouponDiscount = false;
        $CouponItemLevel = substr($discountInfo['ChargeDisplayName'],0,6);
        $CouponTranLevel = substr($discountInfo['ChargeDisplayName'],12,6);
        if($CouponItemLevel == 'Coupon' or $CouponTranLevel == 'Coupon') $IsCouponDiscount = true;

        //Employee Values
        $Employee = substr($discountInfo['ChargeDisplayName'],0,8);
        $IsEmployeeDiscount = false;
        if($Employee == 'Employee') $IsEmployeeDiscount = true;

        //Check if there is a valid reduction
        if( empty($this->Reduction)) { $this->Reduction = 0;}
        if($this->Reduction == 0) return array($discountTag = [], false);

        //If theres no discount type, return
        if(empty($this->DiscountType)) return array($discountTag = [], false);

        //Check Type of Discount
        $IsTranLevel = ($this->DiscountType == "Transaction") ? true : false;
        $IsPercentageDiscount = (!empty($IsPercentageDiscount)) ? true : false;
        $this->IsTranLevel = null;
        $this->IsItemLevel = null;


        //Set the discount Idx
        if($IsTranLevel)
        {
            //This is a Transaction Level Discount
            $this->IsTranLevel = "1";
            $this->Idx = ($IsPercentageDiscount) ? "7" : "8";
            $CouponIdx = ($IsPercentageDiscount) ? "9" : "10";
        }
        else
        {
            //This is an Item Level Discount
            $this->IsItemLevel = "1";
            $this->Idx = ($IsPercentageDiscount) ? "1" : "2";
            $CouponIdx = ($IsPercentageDiscount) ? "3" : "4";
        }

        //Check for a Coupon
        if($IsCouponDiscount == true)
        {
            //Create the Discount Tag
            $discountTag = [
                "DISCOUNT" =>  [
                    "IsTranLevel" => $this->IsTranLevel,
                    "IsItemLevel" => $this->IsItemLevel,
                    "Reduction" => $this->Reduction,
                    "Idx" => $CouponIdx,
                    "GENINFO" =>
                    [
                        "Name" => "UserInputs",
                        "FIELD" =>
                        [
                            "Data" => $discountInfo['Comments'],
                            "Name" => "Coupon_No"
                        ]
                    ]
                ]
            ];

            return array($discountTag, false);
        }

        //Check for a rewards discount
        if($IsRewardsDiscount == true)
        {
            //Create the Discount Tag
            $discountTag = [
                "DISCOUNT" =>  [
                    "IsTranLevel" => $this->IsTranLevel,
                    "IsItemLevel" => $this->IsItemLevel,
                    "Reduction" => $this->Reduction,
                    "Idx" => "18"
                ]
            ];

            return array($discountTag, false);
        }

        //Check for a employee discount
        if($IsEmployeeDiscount == true)
        {
            //Create the Discount Tag
            $discountTag = [
                "DISCOUNT" =>  [
                    "IsTranLevel" => $this->IsTranLevel,
                    "IsItemLevel" => $this->IsItemLevel,
                    "Reduction" => $this->Reduction,
                    "Idx" => "6"
                ]
            ];

            return array($discountTag,true);
        }
        

        //Get the Discount Code
        $this->Code = ltrim(substr($discountInfo['Reason']['ReasonId'],4,4),'0');

        //Check for CFL Bulb and Promotions
        if($discountInfo['ChargeDisplayName'] == "CA Title 20 Bulb" or $discountInfo['ChargeType']['ChargeTypeId'] == "Promotion")
        {
            $this->Idx = "17";
            $this->Code = "1";
        } 
        
        //Create the Discount Tag
        $discountTag = [
            "DISCOUNT" =>  [
                "IsTranLevel" => $this->IsTranLevel,
                "IsItemLevel" => $this->IsItemLevel,
                "Reduction" => $this->Reduction,
                "Idx" => $this->Idx,
                "REASON" => [
                    "Code" => $this->Code
                ]
            ]
        ];
        
        return array($discountTag, false);
    }

    private function multiDiscountTag($discountInfo)
    {
        //Discount Codes
        //  1 = Generic Item Percent Discount
        //  2 = Generic Item Dollar Discount
        //  3 = Coupon Percent Discount - item
        //  4 = Coupon Dollar Discount - item
        //  5 = Re-stocking Fee
        //  6 = Employee Discount
        //  7 = Generic Transaction Percent Discount
        //  8 = Generic Transaction Dollar Discount
        //  9 = Coupon Percent Discount - transaction level
        // 10 = Coupon Dollar Discount - transaction level
        // 11 = NOT USED
        // 12 = Merchandise Discount Card
        // 13 = Coupon Discount ('QP' type, validated) - item level
        // 14 = Coupon Dollar Discount ('QP' type, validated) - tran. level
        // 15 = Coupon Percent Discount ('QP' type, validated) - tran. level
        // 16 = Paper Merchandise Discount
        // 17 = Free CFL Bulb
        // 18 = Rewards Customer Discount on discountinued items

        //Generate the multi-discount tag
        $y = 0;
        $DiscountGroup = 
        [
            "7" => 1,
            "8" => 2,
            "9" => 3,
            "10" => 4
        ];
        foreach($discountInfo as $discount)
        {
            //Get the discount values
            $this->DiscountType = substr($discount['ChargeDisplayName'],0,11);
            $IsPercentageDiscount = ltrim($discount['ChargePercent'],'0');
            $this->Reduction = ltrim($discount['ChargeTotal'],'0');

            //Reward Values
            $Rewards = substr($discount['ChargeDisplayName'],0,6);
            $IsRewardsDiscount = false;
            if($Rewards == 'Reward') $IsRewardsDiscount = true;

            //Coupon Values
            $IsCouponDiscount = false;
            $CouponItemLevel = substr($discount['ChargeDisplayName'],0,6);
            $CouponTranLevel = substr($discount['ChargeDisplayName'],12,6);
            if($CouponItemLevel == 'Coupon' or $CouponTranLevel == 'Coupon') $IsCouponDiscount = true;

            //Employee Values
            $Employee = substr($discount['ChargeDisplayName'],0,8);
            $IsEmployeeDiscount = false;
            if($Employee == 'Employee') $IsEmployeeDiscount = true;
            
            //Check if there is a valid reduction
            if( empty($this->Reduction)) { $this->Reduction = 0;}
            if($this->Reduction == 0) continue;
            
            //If theres no discount type, return
            if(empty($this->DiscountType)) continue;

            //Check Type of Discount
            $IsTranLevel = ($this->DiscountType == "Transaction") ? true : false;
            $IsPercentageDiscount = (!empty($IsPercentageDiscount)) ? true : false;
            $this->IsTranLevel = null;
            $this->IsItemLevel = null;


            //Set the discount Idx
            if($IsTranLevel)
            {
                //This is a Transaction Level Discount
                $this->IsTranLevel = "1";
                $this->Idx = ($IsPercentageDiscount) ? "7" : "8";
                $CouponIdx = ($IsPercentageDiscount) ? "9" : "10";
            }
            else
            {
                //This is an Item Level Discount
                $this->IsItemLevel = "1";
                $this->Idx = ($IsPercentageDiscount) ? "1" : "2";
                $CouponIdx = ($IsPercentageDiscount) ? "3" : "4";
            }

            //Get the Discount Code
            $this->Code = ltrim(substr($discount['Reason']['ReasonId'],4,4),'0');

            //Check for CFL Bulb and Promotions
            if($discount['ChargeDisplayName'] == "CA Title 20 Bulb" or $discount['ChargeType']['ChargeTypeId'] == "Promotion")
            {
                $this->Idx = "17";
                $this->Code = "1";
            }
            
            

            //Create the Discount Tag
            //Check for a Normal Discount, Rewards Discount or Coupon Discount 
            //***Normal Discount***
            if($IsRewardsDiscount == false and $IsCouponDiscount == false)
            {
                //This is a Normal Discount Discount   
                $discountTag["DISCOUNT{$y}"] = 
                [
                    "IsTranLevel" => $this->IsTranLevel,
                    "IsItemLevel" => $this->IsItemLevel,
                    "Reduction" => $this->Reduction,
                    "Idx" => $this->Idx,
                    "REASON" =>
                    [
                        "Code" => $this->Code
                    ]
                ];
            }

            //***Coupon Discount***
            if($IsCouponDiscount == true)
            {
                //This is a Coupon Discount
                $discountTag["DISCOUNT{$y}"] = 
                [
                    "IsTranLevel" => $this->IsTranLevel,
                    "IsItemLevel" => $this->IsItemLevel,
                    "Reduction" => $this->Reduction,
                    "Idx" => $CouponIdx,
                    "GENINFO" =>
                    [
                        "Name" => "UserInputs",
                        "FIELD" => 
                        [
                            "Data" => $discount['Comments'],
                            "Name" => "Coupon_No"
                        ]
                    ]
                ];
            }

            //***Rewards Discount***
            if($IsRewardsDiscount == true)
            {
                //This is a rewards discount
                $discountTag["DISCOUNT{$y}"] = 
                    [
                        "IsTranLevel" => $this->IsTranLevel,
                        "IsItemLevel" => $this->IsItemLevel,
                        "Reduction" => $this->Reduction,
                        "Idx" => "18"
                    ];
            }

            //***Employee Discount***
            if($IsEmployeeDiscount == true)
            {
                //This is a rewards discount
                $discountTag["DISCOUNT{$y}"] = 
                    [
                        "IsTranLevel" => $this->IsTranLevel,
                        "IsItemLevel" => $this->IsItemLevel,
                        "Reduction" => $this->Reduction,
                        "Idx" => "6"
                    ];
            }

            $y++;
        }

        return array($discountTag,$IsEmployeeDiscount);
    }

    private function hasArPayment($payments)
    {
        //Check if this is valid payment information/valid array
        if(! is_array($payments)) return false;

        foreach($payments as $payment)
        {
            if($payment['PaymentType']['PaymentTypeId'] == 'AR') return true;
        }
        return false;
    }

    private function arCheck($payments, $CustomerNumber, $type)
    {
        foreach($payments as $payment)
        {
            if($payment['PaymentType']['PaymentTypeId'] == 'AR')
            {
                $arTags = $this->ARTags($payment,$CustomerNumber,$type);
            } 
        }
        return $arTags['GENINFO'];
    }

    private function ARTags($payment,$CustomerNumber,$type)
    {
        //Set Values
        $ARData = $payment['PaymentMethodAttribute'];
        $FirstName = null;
        $LastName = null;
        $LotNumber = null;
        $PONumber = null;
        $ProjectName = null;
        $PurchaserTitle = null;
        $y = 0;

        //Cycle through the AR Info and gather information for each tag
        foreach($ARData as $index => $info)
        {
            //Gather AR Fields
            switch ($info['Name']) {
                case "FirstName":
                    $FirstName = $info['Value'];
                    break;
                case "LastName":
                    $LastName = $info['Value'];
                    break;
                case "PONumber":
                    $PONumber = $info['Value'];
                    break;
                case "PurchaserTitle":
                    $PurchaserTitle = $info['Value'];
                    break;
            }
        }

        //Create the Customer Details AR Tag
        if($type == 'Customer')
        {
            $arTag["GENINFO"] = 
                [
                    "Name" => "ARCustomerDetails",
                    "FIELD1" => 
                    [
                        "Data" => $CustomerNumber,
                        "Name" => "CustomerNumber"
                    ],
                    "FIELD2" => 
                    [
                        "Data" => $FirstName,
                        "Name" => "FirstName"
                    ],
                    "FIELD3" => 
                    [
                        "Data" => $LastName,
                        "Name" => "LastName"
                    ],
                    "FIELD4" => 
                    [
                        "Data" => $CustomerNumber,
                        "Name" => "AccountNumber"
                    ],
                    "FIELD5" => 
                    [
                        "Data" => $PONumber,
                        "Name" => "PONumber"
                    ]
                ];
        }

        //Create the Purchaser Info AR Tag
        if($type == 'Purchaser')
        {
            $arTag["GENINFO"] = 
                [
                    "Name" => "PurchaserInfo",
                    "FIELD1" => 
                    [
                        "Data" => "{$FirstName} {$LastName}",
                        "Name" => "PURCHASER"
                    ],
                    "FIELD2" => 
                    [
                        "Data" => $PurchaserTitle,
                        "Name" => "TITLE"
                    ]
                ];
        }
            

        return $arTag;
    }

    private function tenderTags($data,$tradeTags,$payments,$customerInfo)
    {
        // Tender Codes
        $tenderTypes =  
        [
            'Cash' => '1',
            'Check' => '2',
            'Manager Approved Check' => '2', 
            'Debit' => '3',
            'Gift Card' => '4',
            'Paper Gift Certificate' => '6',
            'Visa' => '7',
            'MasterCard' => '8',
            'Amex' => '9',
            'Discover' => '10',
            'Credit Memo' => '11',
            'CFO' => '15',
            'Diners' => '18',
            'Store Card' => '26',
            'AR' => '29'
        ];

        $giftCardCount = 1;
        $indexCount = 0;

        // 1 - Cash (600)
        // 2 - Check (601)
        // 3 - Debit (629)
        // 4 - Gift Card (614)
        // 5 - House Account (???)
        // 6 - Gift Certificate (612)
        // 7 - Visa (630)
        // 8 - Mastercard (624)
        // 9 - Amex (625)
        // 10 - Discover (626)
        // 11 - Credit Memo (616)
        // 12 - Canadian Cash (???)
        // 13 - Coupon (???)
        // 14 - Credit Note Issue (???)
        // 15 - Check From Office (619)
        // 16 - Travelers Check (603)
        // 17 - JCB (628)
        // 18 - Diners (627)
        // 19 - Mexican Peso (???)
        // 20 - Euro (???)
        // 21 - British Pound (???)
        // 22 - Mall Certificate (631)
        // 23 - Japanese Yen (???)
        // 24 - Manager Approval Check (632)
        // 25 - Bank Voucher (622)
        // 26 - Store Card (618)
        // 27 - Accts Receivable (620)
        // 28 - Bank Voucher Return (622)
        // 50 - Bid Card (9009)
        // 29 - Accounts Receivable (620)


        foreach($payments as $index => $payment)
        {
            //Initalize Fields
            $IsCredit = null;
            $IsCash = null;
            $IsChange = null;
            $IsDebit = null;
            $IsCheck = null;
            $IsIn = null;
            $IsOut = null;
            $IsKeyed = null;
            $IsValueCert = null;
            $IsValueCard = null;
            $IsVoucher = null;
            $SerialNum = null;
            $ExpDate = null;
            $AuthNum = null;
            $PS2000Data = null;
            $Acct = null;
            $Idx = null;
            $Signature = null;
            $ChangeAmt = null;
            $Token = null;
            $Time = $this->localizeTimestamp((new Carbon($payment['CreatedTimestamp'], 'GMT')), 'Y-m-d\\TH:i:s');
            $LocationCode = null;
            $TransitNum = null;
            $MicrNum = null;
            $DLCCNum = null;
            $SeqNumber = null;
            $GENINFO = null;

            //Check if tender failed
            if($payment['PaymentTransaction']['0']['PaymentResponseStatus']['PaymentResponseStatusId'] == "Failure") continue;

            //Get the Payment Type
            $PaymentType = $payment['PaymentType']['PaymentTypeId'];

            //Check if valid credit card
            //if($PaymentType == 'Credit Card' and $payment['AccountNumber'] == null) continue;

            //If tender is voided, continue to next tender
            if($payment['IsVoided']) continue;

            //If tender is the orignal settlement, continue to next tender
            if($payment['IsCopied'] and $payment['CurrentRefundAmount'] == 0) continue;

            //Ignore Gift Card Tender when buying a gift card, Settled Amount is 0
            if($payment['IsModifiable'] and $payment['CurrentSettledAmount'] == 0 and $payment['CurrentRefundAmount'] == 0 and $PaymentType == 'Gift Card')
            {
                $tradeTags["TENDERGIFTCARD{$giftCardCount}"] = 
                [
                    "Acct" => (!empty($payment['AccountNumber']) ? $payment['AccountNumber'] : null)
                ];
                $giftCardCount++;
                continue;
            }       
                
            //Get the Amount
            $Amt = $payment['Amount'];

            //If Credit Card, and this is a refund, use the refunded amount 
            if($PaymentType == 'Credit Card' and $payment['CurrentRefundAmount'] > 0) $Amt = $payment['CurrentRefundAmount'] * -1;

            //Check if this is an In Tender or Out Tender
            if($Amt > 0) $IsIn = "1";
            if($Amt < 0) $IsOut = "1";

            //Create Tender Tag Fields
            if($PaymentType == 'Cash')
            {
                //This is a cash transaction
                $ChangeAmt = $payment['ChangeAmount'];
                if($ChangeAmt > 0 and $Amt > 0) $Amt = $Amt + $ChangeAmt;
                $IsCash = "1";
                $Idx = $tenderTypes[$PaymentType];
            }
            elseif($PaymentType == 'Check')
            {
                //This is a check transaction
                $LocationCode = $data['CustomerIdentityDoc']['0']['State'];
                $TransitNum = $payment['RoutingNumber'];
                $Acct = $payment['AccountNumber'];
                $IsCheck = "1";
                $DLCCNum = $data['CustomerIdentityDoc']['0']['IdDocNumber'];
                $AuthNum = $payment['PaymentTransaction']['0']['ReconciliationId'];
                $Idx = $tenderTypes[$PaymentType];
                $SeqNumber = $payment['CheckNumber'];
                $MicrNum = "t{$TransitNum}t {$Acct}o{$SeqNumber}";
                
                $GENINFO = 
                [
                    "Name" => "UserInputs",
                    "FIELD" => 
                    [
                        "Data" => $customerInfo['PhoneNumber'],
                        "Name" => "PHONE_NUM"
                    ]
                ];
            }
            elseif($PaymentType == 'Manager Approved Check')
            {
                //This is a check transaction
                $LocationCode = $data['CustomerIdentityDoc']['0']['State'];
                $TransitNum = $payment['RoutingNumber'];
                $Acct = $payment['AccountNumber'];
                $IsCheck = "1";
                $DLCCNum = $data['CustomerIdentityDoc']['0']['IdDocNumber'];
                $AuthNum = $payment['PaymentMethodAttribute']['0']['Value'];
                $Idx = $tenderTypes[$PaymentType];
                $SeqNumber = $payment['CheckNumber'];
                $MicrNum = "t{$TransitNum}t {$Acct}o{$SeqNumber}";
                
                $GENINFO = 
                [
                    "Name" => "UserInputs",
                    "FIELD" => 
                    [
                        "Data" => $customerInfo['PhoneNumber'],
                        "Name" => "PHONE_NUM"
                    ]
                ];
            }
            elseif($PaymentType == 'AR')
            {
                //This is an A/R Transaction
                $Acct = $payment['AccountNumber'];
                $Idx = $tenderTypes[$PaymentType];
                $IsKeyed = "1";
                $IsValueCert = "1";
                $IsVoucher = "1";
            }
            elseif($PaymentType == 'Credit Memo')
            {
                //This is a Credit Memo Transaction
                $SerialNum = $payment['PaymentMethodAttribute']['0']['Value'];
                $Idx = $tenderTypes[$PaymentType];
                $IsKeyed = "1";
                $IsValueCert = "1";
                $IsVoucher = "1";
            }
            elseif($PaymentType == 'Paper Gift Certificate')
            {
                //This is a Gift Certificate Transaction
                $SerialNum = $payment['AccountNumber'];
                $Idx = $tenderTypes[$PaymentType];
                $IsKeyed = "1";
                $IsValueCert = "1";
                $IsVoucher = "1";
            }
            elseif($PaymentType == 'CFO')
            {
                //This is a Check From Office Transaction
                $Idx = $tenderTypes[$PaymentType];
                $IsKeyed = "1";
                $IsValueCert = "1";
                $IsVoucher = "1";
                $cfoLastName = null;
                $cfoFirstName = null;
                $cfoAddress = null;
                $cfoCity = null;
                $cfoState = null;
                $cfoZipcode = null;
                $cfoPhone = null;
                
                foreach($payment['PaymentMethodAttribute'] as $cfoInfo)
                {
                    //Gather CFO Fields
                    switch ($cfoInfo['Name']) {
                        case "LastName":
                            $cfoLastName = $cfoInfo['Value'];
                            break;
                        case "FirstName":
                            $cfoFirstName = $cfoInfo['Value'];
                            break;
                        case "Address1":
                            $cfoAddress = $cfoInfo['Value'];
                            break;
                        case "City":
                            $cfoCity = $cfoInfo['Value'];
                            break;
                        case "State":
                            $cfoState = $cfoInfo['Value'];
                            break;
                        case "PostalCode":
                            $cfoZipcode = is_numeric($cfoInfo['Value']) ? $cfoInfo['Value'] : null;
                            break;
                        case "Phone":
                            $cfoPhone = is_numeric($cfoInfo['Value']) ? $cfoInfo['Value'] : null;
                            break;
                    }
                }

                $GENINFO = 
                [
                    "Name" => "UserInputs",
                    "FIELD1" => 
                    [
                        "Data" => $cfoLastName,
                        "Name" => "OFFICECHECK_LAST_NAME"
                    ],
                    "FIELD2" => 
                    [
                        "Data" => $cfoFirstName,
                        "Name" => "OFFICECHECK_FIRST_NAME"
                    ],
                    "FIELD3" => 
                    [
                        "Data" => $cfoAddress,
                        "Name" => "ADDRESS"
                    ],
                    "FIELD4" => 
                    [
                        "Data" => $cfoCity,
                        "Name" => "CITY"
                    ],
                    "FIELD5" => 
                    [
                        "Data" => $cfoState,
                        "Name" => "STATE"
                    ],
                    "FIELD6" => 
                    [
                        "Data" => $cfoZipcode,
                        "Name" => "ZIPCODE"
                    ],
                    "FIELD7" => 
                    [
                        "Data" => $cfoPhone,
                        "Name" => "PHONE"
                    ]
                ];
            }
            elseif($PaymentType == 'Store Card')
            {
                
                //This is a Store Card Transaction
                $Acct = $payment['AccountNumber'];
                //$AuthNum = $payment['PaymentTransaction']['0']['ReconciliationId'];
                $Idx = $tenderTypes[$PaymentType];
                $IsKeyed = "1";
                $IsValueCard = "1";

                if(isset($customerInfo['RewardsNumber']))
                {
                    $GENINFO = 
                    [
                        "Name" => "UserInputs",
                        "FIELD1" => 
                        [
                            "Data" => substr($customerInfo['FirstName'],0,20),
                            "Name" => "FIRST_NAME"
                        ],
                        "FIELD2" => 
                        [
                            "Data" => $customerInfo['LastName'],
                            "Name" => "LAST_NAME"
                        ],
                        "FIELD3" => 
                        [
                            "Data" => $customerInfo['PhoneNumber'],
                            "Name" => "PHONE"
                        ]
                    ];
                }
            }
            else
            {
                //This is a card transaction
                //Debit or Credit or Gift Card?
                switch ($PaymentType) {
                    case "Debit":
                        $IsDebit = "1";
                        $ExpDate = "20{$payment['CardExpiryYear']}-{$payment['CardExpiryMonth']}-01";
                        $AuthNum = $payment['PaymentTransaction']['0']['ReconciliationId'];
                        $Acct = $payment['AccountNumber'];
                        $PS2000Data = "{$payment['PaymentTransaction']['0']['RequestId']}";
                        $Token = $payment['PaymentTransaction']['0']['RequestToken'];
                        $Idx = $tenderTypes[$PaymentType];
                        $Signature = $payment['CustomerPaySignature'];
                        break;
                    case "Gift Card":
                        $Amt = $Amt;
                        $Acct = $payment['AccountNumber'];
                        $Idx = $tenderTypes[$PaymentType];
                        $IsValueCard = "1";
                        $IsKeyed = "1";
                        break;
                    case "Credit Card":
                        $cardType = $payment['CardType']['CardTypeId'];
                        $IsCredit = "1";
                        $expirationYear = $payment['CardExpiryYear'];
                        $expirationMonth = $payment['CardExpiryMonth'];
                        if(is_null($expirationYear)){$expirationYear = "99";}
                        if(is_null($expirationMonth)){$expirationMonth = "12";}
                        $Acct = $payment['AccountNumber'];
                        $ExpDate = "20{$expirationYear}-{$expirationMonth}-01";
                        $AuthNum = $payment['PaymentTransaction']['0']['ReconciliationId'];
                        $PS2000Data = "{$payment['PaymentTransaction']['0']['RequestId']}";
                        $Token = (!empty($payment['AccountNumber']) ? $payment['AccountNumber'] : null);
                        $Idx = $tenderTypes[$cardType];
                        $Signature = $payment['CustomerPaySignature'];
                        break;
                }
            }

            //Create the Tender Tag
            $tradeTags["TENDER{$indexCount}"] = 
            [
                "IsCredit" => $IsCredit,
                "PS2000Data" => $PS2000Data,
                "LocationCode" => $LocationCode,
                "TransitNum" => $TransitNum,
                "IsOut" => $IsOut,
                "Acct" => $Acct,
                "MicrNum" => $MicrNum,
                "Amt" => $Amt,
                "SerialNum" => $SerialNum,
                "ExpDate" => $ExpDate,
                "Idx" => $Idx,
                "SeqNumber" => $SeqNumber,
                "AuthNum" => $AuthNum,
                "IsCheck" => $IsCheck,
                "IsCash" => $IsCash,
                "IsDebit" => $IsDebit,
                "IsChange" => $IsChange,
                "IsValueCard" => $IsValueCard,
                "IsKeyed" => $IsKeyed,
                "IsValueCert" => $IsValueCert,
                "IsVoucher" => $IsVoucher,
                "Time" => $Time,
                "IsIn" => $IsIn,
                "Token" => $Token,
                "DLCCNum" => $DLCCNum,
                "Signature" => $Signature,
                "GENINFO" => $GENINFO
            ];

            $indexCount++;
            
            //Is there Cash Change? If Yes, add tender tag
            if($ChangeAmt > 0 and $Amt > 0)
            {
                //Create the Tender Tag
                $tradeTags['TENDERChange'] = 
                [
                    "Amt" => $ChangeAmt,
                    "IsChange" => "1",
                    "Idx" => "1",
                    "IsCash" => "1"
                ];
            }


            
        }

        

        return $tradeTags;
    }

    private function customerInfo($data)
    {
        return [
            "FirstName" => $data['CustomerFirstName'],
            "LastName" => $data['CustomerLastName'],
            "FullName" => "{$data['CustomerFirstName']} {$data['CustomerLastName']}",
            "PhoneNumber" => $data['CustomerPhone'],
            "Email" => $data['CustomerEmail'],
            "RewardsNumber" => empty($data['CustomerId']) ? $data['LoyaltyNumber'] : $data['CustomerId']
        ];
    }

}