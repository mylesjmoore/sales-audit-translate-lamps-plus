<?php

namespace Lamps\Transactions;

class BackOfficeTransaction extends Transaction
{
    public function __construct($data,$table)
    { 
        
        parent::__construct($data,$table);

        $this->transactionId = ltrim(substr($data['TransactionID'],15,4),'0');
        $this->store = ltrim(substr($data['TransactionID'],8,4),'0');
        $this->operator = ltrim($data['OperatorId'], '0');
        $this->registerId = ltrim($data['RegisterID'],'0');
        $this->tranNumber = $data['TransactionID'];

        $this->tags = [
            "Op" => $this->operator,
            "WkStn" => $this->registerId,
            "Tran" => $this->transactionId,
            "Store" => $this->store,            
            "Time" => $this->createdAt,
            "OrderId" => $this->tranNumber
        ];
    }
}