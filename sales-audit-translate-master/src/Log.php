<?php

namespace Lamps;

/**
 * log helper class
 */
class Log
{
    protected $logDirectory;
    protected $processName;
    protected $logFileName;

    protected $timestamp;
    protected $date;
    
    protected $scriptName;
    protected $scriptDirectory;

    /**
     * create the logger
     */
    public function __construct()
    {
        $this->date = date("Y-m-d");
        $this->timestamp = date("Y-m-d.H:i:s");

        $this->logDirectory = getenv('LOG_DIR');
        $this->processName = (isset($GLOBALS['processName'])) ? $GLOBALS['processName'] : '';

        //file: {log directory}{process name}{date}.log
        $this->logFileName = $this->logDirectory.$this->processName."_".$this->date.".log";

        $this->scriptName = (isset($_SERVER['SCRIPT_NAME'])) ? $_SERVER['SCRIPT_NAME'] : '';
        $this->scriptDirectory = (isset($_SERVER['PWD'])) ? $_SERVER['PWD'] : '';
    }

    /**
     * log a notice to the file
     *
     * @param string $message the message to log
     * @return void
     */
    public static function notice($message)
    {
        (new log)->logError($message,"NOTICE");
    }

    /**
     * log a warning to the file
     *
     * @param string $message the message to log
     * @return void
     */
    public static function warning($message)
    {
        (new log)->logError($message,"WARNING");
    }

    /**
     * log an error to the file
     *
     * @param string $message the message to log
     * @return void
     */
    public static function error($message)
    {
        (new log)->logError($message,"ERROR");
    }

    /**
     * Append a message to a log file.
     *
     * @param string $message the message to log
     * @param string $level the error level to log
     * @return void
     */
    protected function logError($message,$level)
    {
        error_log("[ {$this->timestamp} ] [ $level ] ".$message.PHP_EOL,3, $this->logFileName);
    }
}