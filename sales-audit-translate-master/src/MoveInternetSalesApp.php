<?php

namespace Lamps;

use Lamps\ErrorHandler;
use Lamps\Database\Connection;
use Lamps\Database\Query;
use Lamps\Email;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class MoveInternetSalesApp
{
    // This Class will do the following:
    // 1) scrub the data of bad characters (specifically the I record)
    // 2) log pertinent info about each transaction in each file
    // 3) move the sales to the S.A. PollData folder
    // 4) change the folder name from "AWx.9999999" to "AWxI.99999999"
    // 5) clean up Batchwriter mistakes, i.e. empty folders and dropping Qtee file

    // The processing flow is as follows:
    // 1) get the list of AW folders to process (1 or more)
    // 2) for each valid folder found, process each XPOLLD store file
    // 3) for each XPOLLD store file, process every record
    // 4) handle each record type separately, i.e. collect date or scrub data
    // 5) output the processed file to the new folder 
    
    protected $errorHandler;
    protected $query;

    protected $batchNumber;
    protected $tranLog = [];
    protected $tranCnt = 0;
    protected $folderIsEmpty;
    
    public function __construct()
    {

        $this->query = new Query(Connection::make());

        $this->fileSystem = new Filesystem();

        // get relavent folder paths & other environment variables
        $this->InetSalesDir = getenv('INET_SALES_INPUT');
        $this->SalesAuditPolldata = getenv('INET_SALES_OUTPUT');
        $this->FolderPrefix = getenv('FOLDER_PREFIX');

        date_default_timezone_set("America/Los_Angeles");

    }

    public function run()
    {
        // get the list of folder(s) to process thru
        $folderList = scandir($this->InetSalesDir);

        // loop thru each folder found in InetSales directory
        foreach ($folderList as $folderName){

            if($this->validFolderName($folderName))
            {
                $this->ProcessFolder($folderName);
            }
 
        }
       
    }

    public function validFolderName($folderName)
    {
        $this->foldername = $folderName;
        // scandir function also returns entries to parent folders
        switch (substr($folderName,0,2)) {
            case '. ':
                return false;
            case '..':
                return false;
            case 'AW':
                // check for proper folder prefix, AWT (test) or AWL (live)
                if(substr($folderName,0,3) == $this->FolderPrefix)
                {
                    // check for .IP extension so as to not collide with Web Dev Batchwriter
                    // the last thing Batchwriter does is to add the .IP to the folder name
                    if (strpos($folderName,'.IP'))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                
            default:
                return false;;
        }
    }


    public function ProcessFolder($folderName)
    {
        $this->folderName = $folderName;

        // get list of files in folder
        $fileList = scandir($this->InetSalesDir.'/'.$this->foldername);

        // backup sales folder before processing it
        $this->BackupFolder($folderName, $fileList);

        // drop the empty folders Batchwriter is creating by mistake
        if ($this->folderIsEmpty) {
            //delete folder in InetSales
            try { 
                $this->fileSystem->remove($this->InetSalesDir.'/'.$folderName);
            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while deleting the empty AWx folder ";
                Email::internetSales("An error occurred while deleting the old AWx folder ".$exception->getPath());
            }
            return;
        }

        // pull out date-time portion of folder name as "batch number"
        //  sameple folder name:  AWT.04211355.IP
        $len = strpos($folderName,'.',5)-4;
        $this->batchNumber = substr($folderName,4,$len);

        // make a new folder in PollData with correct prefix (AWxI)
        $newPathFolderName = $this->SalesAuditPolldata.$this->FolderPrefix.'I.'.$this->batchNumber;
        try {    
            $this->fileSystem->mkdir($newPathFolderName,true);
            }
         catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating new sales directory at ".$exception->getPath();
            Email::internetSales("An error occurred while creating new sales directory at ".$exception->getPath());
        }

        // loop thru each file found in AWx directory/folder
        foreach ($fileList as $fileName){

            if($this->validFileName($fileName))
            {
                // save transaction data & scrub bad characters
                $newFileData = $this->ProcessFile($fileName);

                // output the new file data array to the new folder
                $outputFile = $this->SalesAuditPolldata.'/'.$this->FolderPrefix.'I.'.$this->batchNumber.'/'.$this->fileName;
                try {
                    file_put_contents($outputFile, $newFileData);
                    }
                 catch (IOExceptionInterface $exception) {
                    echo "An error occurred while writing the sales files to the new folder in PollData, at ".$exception->getPath();
                    Email::internetSales("An error occurred while writing the sales files to the new folder in PollData, at ".$exception->getPath());
                }   
                
                $this->LogTransactionInfoToIBMi();
            }
        }

        // copy inet.DONE file to new folder
        try {
            copy($this->InetSalesDir.'/'.$this->foldername.'/inet.done', $newPathFolderName.'/inet.done');
            }
         catch (IOExceptionInterface $exception) {
            echo "An error occurred while trying to copy .DONE file ".$exception->getPath();
            Email::internetSales("An error occurred while trying to copy .DONE file ".$exception->getPath());
        }
        
        // rename new folder to include .IP suffix, this will cause the file to be processed by S.A.
        try {
            $this->fileSystem->rename($newPathFolderName, $newPathFolderName.'.IP', true);
            }
         catch (IOExceptionInterface $exception) {
            echo "An error occurred while renaming the AWxI folder ".$exception->getPath();
            Email::internetSales("An error occurred while renaming the AWxI folder ".$exception->getPath());
        }
        
        //delete old folder in InetSales
        try { 
            $this->fileSystem->remove($this->InetSalesDir.'/'.$folderName);
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while deleting the old AWx folder ".$exception->getPath();
            Email::internetSales("An error occurred while deleting the old AWx folder ".$exception->getPath());
        }
    }


    public function validFileName($fileName)
    {
        $this->filename = $fileName;
        // scandir function also returns entries to parent folders
        switch (substr($fileName,0,2)) {
            case '. ':
                return false;
                break;
            case '..':
                return false;
                break;
            case 'XP':
                // each store file starts with XPOLLDxxxxx
                if (substr($fileName,0,10)  == 'XPOLLD0601')
                {
                    // skip store 601 (QTee) which is sent by mistake
                    return false;
                }
                else
                {
                    return true;
                }
                break;
            default:
                return false;
        }
    }


    
    public function BackupFolder($folderToBackup, $fileList)
    {    
        $folderName = $folderToBackup;
        //$fileList = $fileList;

        // check for empty folder sometimes created by Batchwriter
        // at a minimum the array will contain "." & ".." as the first two entries
        if (count($fileList) < 3)
        {
            $this->folderIsEmpty = true;
        }
        else
        {
            $this->folderIsEmpty = false;
        }

        // make backup folder in /InetSales/bk
        try {    
            $this->fileSystem->mkdir($this->InetSalesDir.'/bk/'.$folderName,true);
            }
         catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating backup folder at ".$exception->getPath();
            Email::internetSales("An error occurred while creating backup folder at ".$exception->getPath());
        }

        foreach ($fileList as $fileName){
            // copy file to backup folder
            if (substr($fileName,0,2) == 'XP') {
                try {
                    copy($this->InetSalesDir.'/'.$this->foldername.'/'.$fileName, $this->InetSalesDir.'/bk/'.$this->foldername.'/'.$fileName);
                    }
                    catch (IOExceptionInterface $exception) {
                        echo "An error occurred while trying to backup the XP files ".$exception->getPath();
                        Email::internetSales("An error occurred while trying to backup the XP files ".$exception->getPath());
                    }
            }
        }
        Return;
    }



    public function processFile($fileName)
    {
        $this->fileName = $fileName;
        $newFileData = [];
        
        $this->tranCnt = 0;

        // read file contents into array
        $fileData = file($this->InetSalesDir.'/'.$this->foldername.'/'.$this->fileName, FILE_IGNORE_NEW_LINES);
        
        // process each record of every transaction in the file
        foreach ($fileData as $record)
        {
            switch (substr($record,18,1)) 
            {
                case 'H':  // header record  (only one per transaction)
                    $this->CollectHrecData($record);
                    $newFileData[] = $record.hex2bin("0d0a");
                    break;
                case 'I':  // item record
                    $newFileData[] = $this->RemoveIrecBadChar($record).hex2bin("0d0a");
                    break;
                case 'T':  // total record  (only one per transaction)
                    $this->CollectTrecData($record);
                    $this->tranCnt++;  // bump the tran cnt for the next transaction
                    $newFileData[] = $record.hex2bin("0d0a");         
                    break;
                case 'W':  // tender record
                    $newFileData[] = $record.hex2bin("0d0a");
                    break; 
                case 'Z':  // end of file record
                    $newFileData[] = $record.hex2bin("0d0a");
                    break;
                case ' ':  // blank record - ignored
                    break;
                default:
                    //  IOExceptionInterface $exception {
                    echo "found unexpected record type ".$record;     
                    Email::internetSales("found unexpected record type ".$record); //" : ".$exception->getPath());
                    // }
            }
        }
        $this->tranCnt--;  // remove last increment of the count
        return $newFileData;
    }



    // remove non-printable characters from the item description field of the I record
    // in some cases this might shrink the field which is needed. some
    // special charactors take up 2 bytes and the desc. field overruns its
    // 20 bytes pushing the remaining fields out of position.
    public function RemoveIrecBadChar($record)
    {
        $Irec1stPart  = substr($record,0,108);
        $IrecItemDesc = substr($record,108,20);
        // rec. length is either 213 for purchase or 243 for return item             <--- please note !!
        if (strlen($record) < 225) // check for shorter "purchase" item rec. len.
        {
            $IrecLastPart = substr($record,-85,85);  // get last 85 bytes
        }
        else
        {
            $IrecLastPart = substr($record,-115,115); // get last 115 bytes
        }
        $IrecItemDesc = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $IrecItemDesc);
        $record = $Irec1stPart.$IrecItemDesc.$IrecLastPart;
        return $record;
    }



    public function CollectHrecData($record)
    {
        $this->tranLog[$this->tranCnt][0] = $this->batchNumber;   // S.A. batch number
        $this->tranLog[$this->tranCnt][1] = substr($record,4,3);  // store number
        $this->tranLog[$this->tranCnt][2] = substr($record,7,4);  // sub-location
        $this->tranLog[$this->tranCnt][3] = substr($record,12,6); // transaction no
        $this->tranLog[$this->tranCnt][4] = substr($record,31,6); // transaction date
        $this->tranLog[$this->tranCnt][5] = substr($record,37,4); // Transaction time
        return;
    }

    public function CollectTrecData($record)
    {
        // change tran total from  000012345+  to +0000123.45
        $tranTotal = substr($record,38,1).substr($record,29,7).'.'.substr($record,36,2);
        $this->tranLog[$this->tranCnt][6] = $tranTotal;             // transaction total
        $this->tranLog[$this->tranCnt][7] = substr($record,104,21); // online order number
        $this->tranLog[$this->tranCnt][8] = substr($record,125,15); // DOM invoice number
        return;
    }  

    public function LogTransactionInfoToIBMi()
    {
        $sqlValuesText = '';
        $y = 1;

        // build SQL values clause for SQL Insert statement
        for ($x = 0; $x <= $this->tranCnt; $x++) {
            $sqlValuesText = $sqlValuesText."('".$this->tranLog[$x][0]."',";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][1].",";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][2].",";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][3].",";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][4].",";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][5].",";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][6].",'";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][7]."','";
            $sqlValuesText = $sqlValuesText.$this->tranLog[$x][8]."',current_timestamp)";

            if ($x == $this->tranCnt || $y == 100) {
                // insert transaction info in SAINETSALS table in MOMS, max 100 trans at a time
                $this->query->InsertInetSalesLog($sqlValuesText); 
                $y = 1;
                $sqlValuesText = '';
            }
            else
            {
                // as long as there are more transactions, add a comma to the end of the string
                if ($x < $this->tranCnt) {$sqlValuesText = $sqlValuesText.",";}
                $y++;
            }

        }
        
        return;
    }
     

}
