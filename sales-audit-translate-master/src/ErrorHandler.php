<?php

namespace Lamps;

class ErrorHandler
{
	/**
	 * Number of times an error can occur before the process shuts down
	 */
	private $shutdownThreshold;

	/**
	 * Information about errors that have occured during the process
	 *
	 * @var array
	 */
	private $errors = [];

	/**
	 * Register a function that occurs when the process shutsdown
	 * 
	 * Register a function that occurs when an error is encountered
	 */
    public function __construct()
    {

		$this->shutdownThreshold = getenv('ERROR_SHUTDOWN_THRESHOLD');

		//register a function that is called when the process shuts down
	    register_shutdown_function([$this,"fatalErrors"]);

		//register a function that is called when an error occurs
        set_error_handler([$this,"errors"]);
    }

	/**
	 * Handle an error that requires the process to end immediately
	 *
	 * @return void
	 */
    public function fatalErrors()
    {
		$error = error_get_last();

		if($error !== NULL and $error['line'] > 0)
		{
			extract($error);

			Log::error("[FATAL ERROR] [File: {$file}] [Line: {$line}] [Message: {$message}]");

			Email::error("
				<b>A fatal error has occured.</b> <br> 
				This error requires that the process end immediately. <br>
				Any other errors may have not been logged or emailed. <br><br>
				
				<b>File:</b> {$error['file']} <br>
				<b>Line:</b> {$error['line']} <br>
				<b>Description:</b> {$error['message']} <br>
			");
		}
    }

	/**
	 * Handle an error when it is encountered
	 *
	 * @param [type] $errorType - type of error or error severity
	 * @param [type] $errorDescription - description of the error
	 * @param [type] $errorFile - file that the error occured in
	 * @param [type] $errorLine - line the error occured on
	 * @return bool
	 */
    public function errors($errorType, $errorDescription, $errorFile, $errorLine)
	{
		//error type not included in error_reporting, do not handle
		if ( ! (error_reporting() & $errorType)) {return false;}

		$errorKey = md5($errorLine.$errorDescription);

		if($this->reoccuringError($errorKey))
		{
			Log::error("[Type: {$errorType}] [File: {$errorFile}] [Line: {$errorLine}] [Description: {$errorDescription}]");

			$this->errors[$errorKey]['count'] += 1;

			//shutdown if error has occured too many times
			if($this->errors[$errorKey]['count'] >= $this->shutdownThreshold)
			{
				Email::error("
					<b>A reoccuring error has exceeded the shutdown threshold and the process has ended. </b><br><br>

					<b>Type:</b> {$errorType} <br>
					<b>File:</b> {$errorFile} <br>
					<b>Line:</b> {$errorLine} <br>
					<b>Description:</b> {$errorDescription} <br><br>

					Please review the process logs, correct any issues, and restart the process.</b>
				");

				exit(1);
			}

			return true;
		}

		$this->errors[$errorKey]['line'] = $errorLine;
		$this->errors[$errorKey]['description'] = $errorDescription;
		$this->errors[$errorKey]['count'] = 1;
		$this->errors[$errorKey]['message'] = 
		"
			<b>Line:</b> {$errorLine} <br>
			<b>Description:</b> {$errorDescription} <br>
		";

		Log::error("[Type: {$errorType}] [File: {$errorFile}] [Line: {$errorLine}] [Description: {$errorDescription}]");
		
		Email::warning("
			An Error has occured that does not require the process to end.<br><br>

			<b>Type:</b> {$errorType} <br>
			<b>File:</b> {$errorFile} <br>
			<b>Line:</b> {$errorLine} <br>
			<b>Description:</b> {$errorDescription}
		");

		//dont execute php internal error handler
		return true;
	}

	/**
	 * Determine if an error has occured before
	 *
	 * @param [type] $errorKey
	 * @return bool
	 */
	private function reoccuringError($errorKey)
	{
		return isset($this->errors[$errorKey]);
	}
}