<?php

namespace Lamps;

use Lamps\ErrorHandler;
use Lamps\Database\Connection;
use Lamps\Database\Query;
use Lamps\Email;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class MoveSalesDataApplication
{
    protected $errorHandler;
    protected $query;

    public function __construct()
    {

        $this->query = new Query(Connection::make());

        $this->fileSystem = new Filesystem();

        //instead of environment variables - put the directories in the NOTES/config file
        $this->XMLLocation = getenv('SALES_DATA');
        $this->BackupLocation = getenv('SALES_DATA_BACKUP');

        date_default_timezone_set("America/Los_Angeles");

    }

    public function run()
    {
        //Check if EXPORT_ORIG sales data is present in the file
        $salesDataExists = $this->fileSystem->exists([$this->XMLLocation.'EXPORT_ORIG', $this->XMLLocation.'EXPORT_ORIG.GO']);
        
        //Parse Sales Data
        if($salesDataExists)
        {
            $this->fileSystem->rename($this->XMLLocation.'EXPORT_ORIG', $this->XMLLocation.'EXPORT_ORIG.xfer', true);
            $file = file($this->XMLLocation.'EXPORT_ORIG.xfer');
            $this->fileSystem->remove($this->XMLLocation.'EXPORT_ORIG.GO');

            //Insert Sales Data to IBMi
            $transactionCount = $this->query->insertSalesData($file); 
            
            //MOVE TO BK
            copy($this->XMLLocation.'EXPORT_ORIG.xfer', $this->XMLLocation.'/'.$this->BackupLocation.'/EXPORT_ORIG.xfer.'.date("YmdHjs"));       
            $this->fileSystem->remove($this->XMLLocation.'EXPORT_ORIG.xfer');
            //Email::salesData("The Sales Data Application processed {$transactionCount} transactions successfully to the Sales Export File as of " . date('l F jS h:i:s A') .".");
        }
        else
        {
            Email::salesData("The Sales Data Application did not find an EXPORT_ORIG file. No transactions were processed as of " . date('l F jS h:i:s A') .  ". If you receive this message 2-3 times, please verify that the Sales Data Application is working correctly.");
        }

        
    }
}
