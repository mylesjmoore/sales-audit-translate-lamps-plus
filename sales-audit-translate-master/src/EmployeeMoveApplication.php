<?php

namespace Lamps;

use Lamps\Email;
use Lamps\Log;
use Lamps\ErrorHandler;
use Lamps\Database\Query;
use Lamps\Database\Connection;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Lamps\Utilities\XmlUtility;

class EmployeeMoveApplication 
{
    protected $errorHandler;
    protected $query;

    public function __construct()
    {

        $this->query = new Query(Connection::make());

        $this->fileSystem = new Filesystem();

        $this->XMLLocation = getenv('EMPLOYEE_DATA');

        $this->xmlUtility = new XmlUtility;

    }

    public function run()
    {
        $importData = $this->query->employeeImportData()->fetchAll();
          
        $employeeRow = array();
        $replacements = array();

        foreach($importData as $employee => $value)
        {
            $firstName = strstr(trim($value['FIRSTNAME']), ' ', true);
            $middleName = strstr(trim($value['FIRSTNAME']), ' ');
            $employeeXML["employee number=\"{$value['NUMBER']}\" action=\"addupdate\""] =
            [
                'firstName' => $firstName ? str_replace(".", "", trim($firstName)) : str_replace(".", "", trim($value['FIRSTNAME'])),
                'middleName' => $middleName ? str_replace(".", "", trim($middleName)) : str_replace(".", "", trim($value['MIDDLENAME'])),
                'lastName' => str_replace(".", "", trim($value['LASTNAME'])),
                'active' => $value['ACTIVE'],
                'shortName' => $firstName ? $firstName : str_replace(".", "", trim($value['SHORTNAME'])),
                'employmentStatusCode' => $value['EMPLOYMENTSTATUSCODE'],
                'primaryLocationNumber' => $value['PRIMARYLOCATIONNUMBER'],
                'titlePositionCode' => $value['TITLEPOSITIONCODE']
            ];

            $replacements["{$value['NUMBER']}"] = 
            [
                "</employee number=\"{$value['NUMBER']}\" action=\"addupdate\">" => "</employee>"
            ]; 
        };

        $xml = $this->xmlUtility->makeXmlFromArray(
            'employees',
            $employeeXML
        );
        
        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" . $this->xmlUtility->xmlString($xml); 
        foreach($replacements as $replacement => $newValue)
        {
            foreach($newValue as $key => $value)
            {
                $xml = str_replace($key, $value, $xml);
            }
            
        };
         
        $this->fileSystem->appendToFile($this->XMLLocation .'/employee_1_'. date("mdHis"). '.xml',$xml);

    }
}
