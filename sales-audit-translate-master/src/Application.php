<?php

namespace Lamps;

use Lamps\ErrorHandler;
use Lamps\Database\Query;
use Lamps\Database\Connection;
use Lamps\Translators\Translator;

class Application
{
    protected $translator;
    protected $errorHandler;
    protected $query;
    protected $table;
    protected $recordId;

    public function __construct(Translator $translator,$table,$recordId)
    {
        $this->query = new Query(Connection::make());

        $this->translator = $translator;

        $this->table = $table;

        $this->recordId = $recordId;
    }

    public function run()
    {

        $json = $this->query->jsonRecord($this->table,$this->recordId);

        if( ! $json ) return false;
 
        $xml = $this->translator->jsonToXml($json,$this->table,$this->recordId);
        
        if( ! $xml ) return $this->query->setRecordNotProcessed($this->table,$json['id']);
        
        $this->query->updateFileWithXml($this->table,$this->recordId,$xml); 
    
    }

}
