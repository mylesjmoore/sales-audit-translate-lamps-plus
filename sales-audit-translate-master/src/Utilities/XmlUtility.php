<?php

namespace Lamps\Utilities;

use \SimpleXMLElement;

class XmlUtility
{
    public function makeXmlFromArray($root, $array)
    {
        $xml = new SimpleXMLElement("<{$root}></{$root}>");
        
        foreach($array as $key => $value)
        {
            $this->addNode($xml,$key,$value);
        }
        
        return $xml;
    }  

    private function addNode($xml,$key,$value)
    {
        if(is_array($value))            
        {
            //If it is an Item or Discount, Substring off the number
            if(substr($key, 0,4) == "ITEM"){$key = substr($key, 0,4);}
            if(substr($key, 0,8) == "DISCOUNT"){$key = substr($key, 0,8);}
            if(substr($key, 0,6) == "TENDER")
            {
                if(substr($key, 0,12) != "TENDEREXCHNG") $key = substr($key, 0,6);
            }
            if(substr($key, 0,7) == "GENINFO"){$key = substr($key, 0,7);}
            if(substr($key, 0,5) == "FIELD"){$key = substr($key, 0,5);}
            if(substr($key, 0,3) == "POS"){$key = substr($key, 0,3);}

            $child = $xml->addChild($key);

            foreach($value as $key => $value)
            {
                $this->addNode($child,$key,$value);
            }

        } 
        else
        {
            if(! is_null($value)){$xml->addChild($key,htmlspecialchars($value));}
        }
    }

    public function xmlString(SimpleXMLElement $xml)
    {
        return trim($xml->asXML(),'<?xml version="1.0"?>');
    }
}