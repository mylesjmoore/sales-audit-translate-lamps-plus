<?php

namespace Lamps\Controllers;

use Lamps\ErrorHandler;
use Lamps\Log;
use Lamps\Database\Query;
use Lamps\Database\Connection;

class Watcher {

    protected $query;
    protected $processName;
    protected $processEndTime;
    protected $backOfficeTable;
    protected $orderTable;
    protected static $isRunning = true;

    public function __construct($processName)
    {

        $this->processName = strtolower($processName);

        $this->query = new Query(Connection::make());

        $this->processEndTime = \DateTime::createFromFormat('H:i',getenv('PROCESS_END_TIME'));

        $this->backOfficeTable = getenv('BACK_OFFICE_TABLE');

        $this->orderTable = getenv('ORDER_TABLE');

        $this->registerSignals();

    }

    public function watch()
    {

        Log::notice("Starting Process: {$this->processName}");

        switch($this->processName)
        {
            case 'backoffice':
                $this->loop($this->backOfficeTable);
                break;
            case 'order':
                $this->loop($this->orderTable);
                break;
        }

        Log::notice("Ending Process: {$this->processName}");
        
    }

    protected function loop($table)
    {
        do {

            $recordId = $this->query->unprocessedRecordId($table);
            
            if( ! $recordId ) { sleep(1); continue; }
            
            exec("translate translate:{$this->processName} {$recordId} >> watcher.log &");

        } while (new \DateTime < $this->processEndTime and self::$isRunning);
    }

    /**
     * register signals received from outside process
     *
     * @return void
     */
    protected function registerSignals()
    {
        // Used for signals
        pcntl_async_signals(true);

        //route signal to function
        pcntl_signal(SIGTERM, 'self::handleSignal');

    }

    /**
     * handle a signal that is received
     * from linux terminal command: kill
     *
     * @param int $signo
     * @return void
     */
    protected static function handleSignal($signo) 
    {

        switch ($signo) 
        {

            case SIGTERM:

                Log::notice('Received SIGTERM, dying...');
                self::$isRunning = false;
                return;

        }

    }

}