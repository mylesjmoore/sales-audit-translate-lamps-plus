<?php

namespace Lamps\Database;

use \PDO;
use Lamps\Log;
use Lamps\Email;

/**
 * Database Connection Layer
 */
class Connection
{
  
  /**
   *  make a pdo connection to the database
   *  uses odbc connection profile: IBMdb2
   *  application exits on failure with log and email
   *  
   * @return PDO pdo connection to the IBMdb2 profile
   */
    public static function make()
    {

      try { 
        
        return new PDO("odbc:IBMdb2",NULL,NULL,[
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
          ]);

      } catch (\PDOException $e) {

        Log::error("Failed to connect to database: ".$e->getMessage());

        Email::error("
          <b>aws-sqs</b> attempted to connect to the database and failed.<br>
          The application did not start.<br><br>

          <b>Connection Error:</b> ".$e->getMessage()
        );

        exit(1);

      }

    }

}