<?php

namespace Lamps\Database;

use Lamps\Database\QueryBuilder;

/**
 * Query Builder: provides named queries to the database
 *
 */
class Query extends QueryBuilder
{
    
    /**
     * create the query builder - provide a pdo database connection
     *
     * @param \PDO $pdo pdo database connection
     */
    public function __construct(\PDO $pdo)
    {
        parent::__construct($pdo);
    }

    /**
     * get one unprocessed record id from given table
     * if data is found, it it set to staged and returned
     * 
     * @param [type] $table
     * @return string|false
     */
    public function unprocessedRecordId($table)
    {
        try {

            $this->beginTransaction();

            $data = $this->pdo->query("
                SELECT JSONID 
                FROM {$table}
                WHERE PROCFLAG in ('','R')
                FETCH FIRST ROW ONLY
                WITH RS USE AND KEEP EXCLUSIVE LOCKS
            ")->fetch();

            if($data) $this->setRecordStaged($table,$data['JSONID']);

            $this->commit();

        } catch(\PDOException $exception) {

            $this->handlePdoException($exception,$table);

            exit(1);

        }
        
        if ( ! $data ) return false;

        return $data['JSONID'];
    }

    public function getSettleAmounts($store,$businessDate,$register,$table)
    {
        try {
            $data = $this->pdo->query("
                SELECT DISTINCT a.operationtype as TenderType,substr(a.transactionid,13,3) as register, 
                sum(b.amount) as amount, count(*) as count 
                FROM POSBKOFHTL a 
                JOIN POSBKOFDTL b on a.jsonid = b.jsonid
                WHERE businessdate = '{$businessDate}' and substr(a.transactionid,13,3) = {$register} and substr(a.transactionid,11,2) = {$store} and operationtype in ('TILL_PICKUP','TILL_LOAN')
                GROUP BY a.operationtype,substr(a.transactionid,13,3)
            ")->fetchAll();

        } catch(\PDOException $exception) {

            $this->handlePdoException($exception,$table);

            exit(1);

        }
        
        if ( ! $data ) return false;
        return $data;
    }

    public function getCashSummaryAmounts($store,$businessDate,$table)
    {
        try {
            $data = $this->pdo->query("
                SELECT DISTINCT a.operationtype as TenderType,substr(a.transactionid,13,3) as register, 
                sum(b.amount) as amount, count(*) as count 
                FROM POSBKOFHTL a 
                JOIN POSBKOFDTL b on a.jsonid = b.jsonid
                WHERE businessdate = '{$businessDate}' and substr(a.transactionid,11,2) = {$store} and operationtype in ('TILL_PICKUP','TILL_LOAN')
                GROUP BY a.operationtype,substr(a.transactionid,13,3)
            ")->fetchAll();

        } catch(\PDOException $exception) {

            $this->handlePdoException($exception,$table);

            exit(1);

        }
        
        if ( ! $data ) return false;
        return $data;
    }

    public function getTotalPickupAmounts($store,$businessDate,$table)
    {
        try {
            $data = $this->pdo->query("
                SELECT DISTINCT paymenttypeid as TenderType, cardtypeid as CardType, substr(orderid,13,3) as register, 
                sum(amount) as amount, count(*) as count
                FROM POSSOPAYTL
                WHERE businessdate = '{$businessDate}' and substr(orderid,11,2) = {$store} and paymenttypeid <> '' and isvoided = 'false'
                GROUP BY paymenttypeid,cardtypeid,substr(orderid,13,3)
            ")->fetchAll();
            

        } catch(\PDOException $exception) {

            $this->handlePdoException($exception,$table);

            exit(1);

        }
        
        if ( ! $data ) return false;
        return $data;
    }

    public function getPickupAmounts($store,$businessDate,$register,$table)
    {
        try {
            $data = $this->pdo->query("
                SELECT DISTINCT paymenttypeid as TenderType, cardtypeid as CardType, substr(orderid,13,3) as register, 
                sum(amount) as amount, count(*) as count
                FROM POSSOPAYTL
                WHERE businessdate = '{$businessDate}' and substr(orderid,13,3) = {$register} and substr(orderid,11,2) = {$store} and paymenttypeid <> ''
                GROUP BY paymenttypeid,cardtypeid,substr(orderid,13,3)
            ")->fetchAll();
            

        } catch(\PDOException $exception) {

            $this->handlePdoException($exception,$table);

            exit(1);

        }
        
        if ( ! $data ) return false;
        return $data;
    }


    /**
     * update the given table record to staged process flag status
     *
     * @param [type] $table
     * @param [type] $id
     * @return bool
     */
    public function setRecordStaged($table,$id)
    {
        try {

            return $this->pdo->prepare("
                UPDATE {$table}
                SET PROCFLAG = 'S'
                WHERE JSONID = :JSONID
                WITH RS
            ")->execute(['JSONID' => $id]);

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,['JSONID' => $id]);

            exit(1);

        }
    }

    /**
     * update given table at record id to not processed status
     *
     * @param [type] $table
     * @param [type] $id
     * @return bool
     */
    public function setRecordNotProcessed($table,$id)
    {
        try {

            $stm = $this->pdo->prepare("
                UPDATE {$table}
                SET PROCFLAG = 'E'
                WHERE JSONID = :JSONID AND PROCFLAG not in ('D','H')
                WITH NC
            ");

            return $stm->execute(['JSONID' => $id]);

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,['JSONID' => $id]);

            exit(1);

        }

    }

    /**
     * update given table at record id for the error message
     *
     * @param [type] $table
     * @param [type] $id
     * @param [type] $orderID
     * @param [type] $errorMessage
     * @return bool
     */
    public function setNotProcessedErrorMessage($table,$id,$orderID,$errorMessage)
    {
        try {
            $stm = $this->pdo->prepare("
                UPDATE {$table}
                SET XMLERRMSG = '{$errorMessage}'
                WHERE JSONID = :JSONID AND MESSAGEID = :ORDERID
                WITH NC
            ");

            return $stm->execute(['JSONID' => $id,'ORDERID' => $orderID]);

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,['JSONID' => $id]);

            exit(1);

        }

    }

    /**
     * update given table at record id for the dumped transaction message
     *
     * @param [type] $table
     * @param [type] $id
     * @param [type] $dumpMessage
     * @return bool
     */
    public function setDumpedTransactionMessage($table,$id,$dumpMessage)
    {
        try {
            
            $stm = $this->pdo->prepare("
                UPDATE {$table}
                SET (PROCFLAG,XMLERRMSG) = ('D','{$dumpMessage}')
                WHERE JSONID = :JSONID AND SQSMSGTYPE = 'BackOffice'
                WITH NC
            ");

            return $stm->execute(['JSONID' => $id]);

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,['JSONID' => $id]);

            exit(1);

        }

    }

    /**
     * update given table at record id to hold till reconcile
     *
     * @param [type] $table
     * @param [type] $id
     * @return bool
     */
    public function holdTillReconcile($table,$id)
    {
        try {
            
            $stm = $this->pdo->prepare("
                UPDATE {$table}
                SET (PROCFLAG,XMLERRMSG) = ('H','Till Reconcile on hold.')
                WHERE JSONID = :JSONID AND SQSMSGTYPE = 'BackOffice' AND XMLERRMSG <> 'Till Reconcile released for EOD.'
                WITH NC
            ");

            return $stm->execute(['JSONID' => $id]);

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,['JSONID' => $id]);

            exit(1);

        }

    }

    public function tillReconcileStatus($table,$id)
    {
        try {
            $data = $this->pdo->query("
                SELECT DISTINCT PROCFLAG
                FROM {$table}
                WHERE JSONID = {$id} AND SQSMSGTYPE = 'BackOffice'
                WITH NC
            ")->fetchAll();
            

        } catch(\PDOException $exception) {

            $this->handlePdoException($exception,$table);

            exit(1);

        }
        
        if ( ! $data ) return false;
        return $data['0']['PROCFLAG'];
    }

    /**
     * update given table at record id to release till reconcile
     *
     * @param [type] $table
     * @param [type] $id
     * @return bool
     */
    public function releaseTillReconcile($table,$id)
    {
        try {
            
            $stm = $this->pdo->prepare("
                UPDATE {$table}
                SET (PROCFLAG,XMLERRMSG) = ('','Till Reconcile released for EOD.')
                WHERE SQSMSGTYPE = 'BackOffice' AND PROCFLAG = 'H'
                WITH NC
            ");

            return $stm->execute();

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,['JSONID' => $id]);

            exit(1);

        }

    }

    /**
     * update table at given record id with xml and process status 'X' ready with xml
     *
     * @param [type] $table
     * @param [type] $id
     * @param [type] $xml
     * @return void
     */
    public function updateFileWithXml($table,$id,$xml)
    {
        try {
            $stm = $this->pdo->prepare("
                UPDATE {$table}
                SET PROCFLAG = 'X', XMLCLOB = :XML, XMLCRTTMSP = CURRENT_TIMESTAMP
                WHERE JSONID = :JSONID AND PROCFLAG not in ('D', 'H')
                WITH NC
            ");

            $parameters = ['XML' => $xml,'JSONID' => $id];

            return $stm->execute($parameters);

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,$parameters);

            exit(1);

        }

    }

    /**
     * get json data from a given table and record id 
     *
     * @param [type] $table
     * @param [type] $recordId
     * @return array
     */
    public function jsonRecord($table,$recordId)
    {
        $parameters = ['ID' => $recordId];

        try {

            $stmt = $this->pdo->prepare("
                SELECT JSONID, SQSCLQDATA 
                FROM {$table}
                WHERE JSONID = :ID
            ");

            $stmt->execute($parameters);
            
            $data = $stmt->fetch();

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,$table,$parameters);

            exit(1);

        }

        if ( ! $data) return false;

        return [
            'data' => $data['SQSCLQDATA'],
            'id' => $data['JSONID']
        ];
    }

    /**
     * get an array of undefined transactions
     * that have not been acknowledged
     *
     * @return array
     */
    public function undefinedTransactions()
    {
        try {

            $stmt = $this->pdo->query("
                SELECT *
                FROM ZZMYLESM.POSTOPTL
                WHERE RESOLVED <> 'Y'
                ORDER BY JSONID
            ");

            $stmt->execute();
            
            return $stmt->fetchAll();

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,'POSTOPTL');

            exit(1);

        }

    }

    /**
     * update the undefined transaction table
     * set the transaction type acknowledged for a giben operation type
     *
     * @param [type] $operationType
     * @return bool
     */
    public function acknowledgeUndefinedTransactionTypes($operationType)
    {
        try {

            return $this->pdo->exec("
                UPDATE ZZMYLESM.POSTOPTL  
                SET RESOLVED = 'Y'
                WHERE OPERATION = '{$operationType}'
            ");

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,'POSTOPTL');

            exit(1);

        }
    }

    public function getLoyaltyData()
    {
        try {

            $data = $this->pdo->query("
                SELECT *
                From RWXREFPF2
            ")->fetchAll();

            return $data;
        } catch(\PDOException $exception) {

            $this->handlePdoException($exception, 'RWXREFPF2');
            exit(1);
        }
        
    }

    /**
     * insert Loyalty Data from Auditworks/RebuildLoyaltyNumbers
     *
     * @param string $loyaltyData
     */
    public function insertLoyaltyData($loyaltyData) 
    {
        foreach($loyaltyData as $data)
        {
            try {
                $this->pdo->exec("
                INSERT INTO RWTPOSMTL 
                VALUES('FL','80000000000000',{$data},{$data}, ' ', current_timestamp, current_timestamp)
                ");
                
            } catch (\PDOException $exception) {
    
                $this->handlePdoException($exception,'RWTPOSMTL');
    
                exit(1);
    
            }
        }
    }

    /**
     * insert Sales Data from EXPORT_ORIG in to SAEXPORTFL on IBMi
     *
     * @param string $flatData
     * @return bool
     */
    public function insertSalesData($flatData) 
    {
        $recCount = 0;
        foreach($flatData as $data)
        {
            $tran = substr($data,0,12); 
            $controlNum = substr($data,12,3);
            $recType = substr($data,15,1);
            
            //Get Header Count for number of transactions processed
            if($recType == 'H') $recCount++;

            //Sanitize the data
            //Remove any non ascii or special characters (Avoid Memory Allocation Error)
            //For A (Authorization) records specifically, remove characters within the hex value range
            $recData = substr($data,16,483);
            $recData = str_replace("'"," ",$recData);
            $recData = str_replace("\r\n"," ",$recData);
            $recData = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $recData);

            //Removed check for only 'A' records, all data must be checked
            // if($recType == 'A')
            // {
            //     $recData = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $recData);
            // } 
            

            try {
                $this->pdo->exec("
                    INSERT INTO SAEXPORTFL 
                    VALUES('{$tran}','{$controlNum}','{$recType}','{$recData}',' ', current_timestamp)
                ");
    
            } catch (\PDOException $exception) {
                
                $this->handlePdoException($exception,'SAEXPORTFL');
    
                exit(1);
    
            }
        }
        return $recCount;
    }

    

    /**
     * insert a record into the undefined transaction type table
     *
     * @param string $recordId
     * @param string $transactionId
     * @param string $operationType
     */
    public function recordUndefinedTransactionType($recordId,$transactionId,$operationType) 
    {
        try {

            return $this->pdo->exec("
                INSERT INTO ZZMYLESM.POSTOPTL (JSONID, TRANID, PROCESS, REASON, OPERATION, RESOLVED)
                VALUES({$recordId},'{$transactionId}','Back Office','Unknown Transaction Type', '{$operationType}','N')
            ");

        } catch (\PDOException $exception) {

            $this->handlePdoException($exception,'POSTOPTL');

            exit(1);

        }
    }

    /** 
     * get XML that has not been sent to Sales Audit
     */
    public function unProcessedXML()
    {
        try {

            return $this->pdo->query("
            SELECT storeno,
            jsonid,
            xmlclob
                FROM possqshstl
                WHERE procflag = 'X'
                ORDER BY storeno
                ");
        } catch(\PDOException $exception) {

            $this->handlePdoException($exception, 'POSSQSHSTL');
            exit(1);
        }
        
    }

    public function setXMLProcessed($id,$folder)
    {
        try {

            return $this->pdo->query("
            UPDATE possqshstl
            SET procflag = 'P', sabatchnum={$folder}, snt2satmsp = current_timestamp
            WHERE JSONID = {$id}
            WITH NC
            ");
        } catch(\PDOException $exception) {

            $this->handlePdoException($exception, 'POSSQSHSTL');
            exit(1);
        }
    }

    public function employeeImportData()
    {
        try {

            return $this->pdo->query("
            SELECT *
            FROM IMSLSMVW1
            where firstname <> '' and lastname <> '' 
            ORDER by number
            ");
        } catch(\PDOException $exception) {

            $this->handlePdoException($exception, 'IMSLSMVW1');
            exit(1);
        }
        
    }

    /**
    * insert Inet Sales meta data into SAINETSALS on IBMi
    *
    * @param string $values
    */
    public function InsertInetSalesLog($values) 
    {
        try {
            $this->pdo->exec("
                INSERT INTO SAINETSALS (SAIBTCHID,SAISTRNO,SAISBLOCNO,SAITRANNO,
                SAITRANDT,SAITRANTM,SAITRANTOT,SAIORDERNO,SAIDOMINV,SAICRTTMST)
                VALUES{$values}
            ");

        } catch (\PDOException $exception) {
            
            $this->handlePdoException($exception,'SAINETSALS');

            exit(1);
        }
        
        return;
    }

        
}