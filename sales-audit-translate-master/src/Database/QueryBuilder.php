<?php

namespace Lamps\Database;

use Lamps\Log;
use Lamps\Email;

/**
 * Query Builder: provides named queries to the database
 *
 */
class QueryBuilder
{

    /**
     * PDO Database connection
     *
     * @var PDO
     */
    protected $pdo;

    /**
     * create the query builder - provide a pdo database connection
     *
     * @param PDO $pdo pdo database connection
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * begin a database transaction
     *
     * @return bool returns true on success or false on failure
     */
    public function beginTransaction()
    {
        return $this->pdo->beginTransaction();
    }

    /**
     * commit a database transaction
     *
     * @return bool returns true on success or false on failure
     */
    public function commit()
    {
        return $this->pdo->commit();
    }

    /**
     * Check if a table exists
     *
     * @param string $table the table to check if it exists
     * @return bool
     */
    public function tableExists($table)
    {
        try {

            $this->db->query("
                SELECT 1
                FROM {$table}
                FETCH FIRST ROW ONLY
            ");

        } catch (\Exception $e) {

            return false;

        }

        return true;
    }

    /**
     * log and email pdo errors
     *
     * @param \PDOException $exception
     * @param string $table
     * @param array $data
     * @return void
     */
    protected function handlePdoException(\PDOException $exception, $table, $data = [])
    {
        $code = $exception->getCode();
        $file = $exception->getFile(); 
        $line = $exception->getLine(); 
        $message = $exception->getMessage();
        
        array_walk($data, function(&$value,$key) {
            $value = "<b>{$key}: </b>{$value}<br>";
        });

        Log::error("File: {$file} | Line: {$line} | SQL Code: {$code} | PDO Exception: {$message}");

        Email::error("
            <b>SQL PDO Database Exception Thrown</b> <br><br>
            
            <b>The process has ended.</b> <br><br>
            
            <b>Table:</b> {$table} <br>
            <b>SQL Code:</b> {$code} <br> 
            <b>File:</b> {$file} <br> 
            <b>Line:</b> {$line} <br> 
            <b>PDO Exception Message:</b> {$message}<br><br>

            <b>Related Data:</b><br>
        ". implode(' ', $data));
    }
}