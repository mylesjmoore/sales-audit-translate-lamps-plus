<?php

namespace Lamps;

/**
 * Email helper class.
 */
class Email
{
    private $to;
    private $from;
    private $subject;
    private $messageBody;
    private $headers;
    private $hostName;
    private $scriptName;

    /**
     * Create the emailer
     *
     * @param string $message message to send as a string, accepts html tags
     * @param string $subject - email subject, optional
     * @param string $to - email to address - optional
     * @param string $from - email from address - optional
     */
    public function __construct($message, $subject = NULL, $to = NULL, $from = NULL)
    {
        $this->to = $to ?: getenv('EMAIL_TO');
        $this->from = $from ?: getenv('EMAIL_FROM');
        $this->subject = $subject ?: getenv('EMAIL_SUBJECT');

        $this->hostName = isset($_SERVER['HOSTNAME']) ? $_SERVER['HOSTNAME'] : getenv('HOST_NAME');
        $this->scriptName = isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : getenv('SCRIPT_NAME');

        $this->headers =  
            "MIME-Version: 1.0 \r\n" .
            "Content-type: text/html; charset=iso-8859-1 \r\n" .
            "From: {$this->from} \r\n" .
            "Reply-To: NoReply@LPdomain.com \r\n" .
            "X-Mailer: PHP/" . phpversion();

        $this->messageBody = $this->formatMessage($this->messageHeader().$message);
    }

    /**
     * format a message for email. message requires wordwrap
     * and replace deleted hardstop that ends at the end of a line
     *
     * @param string $message the message to format
     * @return string formatted message
     */
    private function formatMessage($message)
    {
        //sanatize message
        $message = wordwrap($message, 70, "\r\n");
        $message = str_replace("\n.", "\n..", $message);

        //return message
        return $message;
    }

    /**
     * send an email
     *
     * @return void
     */
    private function send($to = NULL)
    {
        $this->to = $to ?: getenv('EMAIL_TO');
        mail($this->to, $this->subject, $this->messageBody, $this->headers);
    }

    /**
     * construct a header to add to the beggining of a message
     *
     * @return string the header message to add.
     */
    private function messageHeader()
    {
        return "
            <b>System Name: </b> {$this->hostName} <br>

            <b>Script Name: </b> {$this->scriptName} <br><br>
        ";
    }

    /**
     * send an email that indicates an error has occured
     *
     * @param string $message the message to send
     * @return void
     */
    public static function error($message)
    {
        $email = new Email(
            $message,
            "An Error Occured"
        );

        $email->send();
    }

    /**
     * send a warning email
     *
     * @param string $message the message to send
     * @return void
     */
    public static function warning($message)
    {
        $salesDataEmailRecipient = getenv('SALES_DATA_RECIPIENT');
        $email = new Email(
            $message,
            "Warning"
        );
        
        $email->send($salesDataEmailRecipient);
    }

    /**
     * send a notice email
     *
     * @param string $message the message to send
     * @return void
     */
    public static function notice($message)
    {
        $salesDataEmailRecipient = getenv('SALES_DATA_RECIPIENT');
        $email = new Email(
            $message,
            "Notice"
        );

        $email->send($salesDataEmailRecipient);
    }

    /**
     * send an unprocessed email
     *
     * @param string $message the message to send
     * @return void
     */
    public static function unprocessed($message)
    {
        $email = new Email(
            "$message",
            "Json to XML Transalate Attempted to Translate Undefined Back Office Transactions"
        );

        $email->send();
    }

    /**
     * send an Sales Data notice email
     *
     * @param string $message the message to send
     * @return void
     */
    public static function salesData($message)
    {
        $systemName = getenv('SYSTEM_NAME');
        $salesDataEmailRecipient = getenv('SALES_DATA_RECIPIENT');
        $email = new Email(
            "$message",
            "Sales Data Notification " . "({$systemName})"
        );
        
        $email->send($salesDataEmailRecipient);
    }

    /**
     * send an internet sales email
     *
     * @param string $message the message to send
     * @return void
     */
    public static function internetSales($message)
    {
        $systemName = getenv('SYSTEM_NAME');
        $internetSalesEmailRecipient = getenv('INTERNET_SALES_RECIPIENT');

        $email = new Email(
            "$message",
            "Internet Sales Notification " . "({$systemName})"
        );
        
        $email->send($internetSalesEmailRecipient);
    }
}