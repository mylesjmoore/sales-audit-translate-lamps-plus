<?php

namespace Lamps\Commands;

use Lamps\Database\Connection;
use Lamps\Database\Query;
use Lamps\Email;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ReportAcknowledgeUndefinedCommand extends Command {

    public function configure()
    {
        $this->setName('report:acknowledgeUndefined')
            ->setDescription('Resolve unprocessed messages with a given operation type.')
            ->setHelp("This command will resolve unprocessed messages for the operation type that is provided.")
            ->addArgument('operation_type',InputArgument::REQUIRED,'The operation type to resolve.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $operationType = strtoupper($input->getArgument('operation_type'));

        $query = new Query(Connection::make());

        $io = new SymfonyStyle($input, $output);

        $successful = $query->acknowledgeUndefinedTransactionTypes($operationType);

        if ( ! $successful) 
        {
            $io->error([
                "Failed to mark records resolved for operation type: {$operationType}.",
                "Review table: POSTOPTL"
            ]);
            $io->note('Operation type may not be valid.');
            return;
        }

        $io->success("Resolved unresolved operation type: {$operationType}");
    }
}