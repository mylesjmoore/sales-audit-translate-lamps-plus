<?php

namespace Lamps\Commands;

use Lamps\MoveLoyaltyNumberDataApplication;
//use Lamps\Translators\BackOfficeTranslator;
use Symfony\Component\Console\Command\Command;
//use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoveLoyaltyDataCommand extends Command {

    public function configure()
    {
        $this->setName('move:loyaltyData')
            ->setDescription('Move Loyalty Number Data from auditworks files to the IBMi.');
//            ->addArgument('record_id',InputArgument::REQUIRED,'The table table record id to translate');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $application = new MoveLoyaltyNumberDataApplication();

        $application->run();
    }
}