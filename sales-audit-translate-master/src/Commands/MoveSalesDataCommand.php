<?php

namespace Lamps\Commands;

use Lamps\MoveSalesDataApplication;
//use Lamps\Translators\BackOfficeTranslator;
use Symfony\Component\Console\Command\Command;
//use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoveSalesDataCommand extends Command {

    public function configure()
    {
        $this->setName('move:salesData')
            ->setDescription('Move Sales Data output from Sales Audit to the IBMi.');
//            ->addArgument('record_id',InputArgument::REQUIRED,'The table table record id to translate');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $application = new MoveSalesDataApplication();

        $application->run();
    }
}