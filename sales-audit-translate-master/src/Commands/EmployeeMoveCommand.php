<?php

namespace Lamps\Commands;

use Lamps\EmployeeMoveApplication;
//use Lamps\Translators\BackOfficeTranslator;
use Symfony\Component\Console\Command\Command;
//use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmployeeMoveCommand extends Command {

    public function configure()
    {
        $this->setName('move:employeeXML')
            ->setDescription('Move Employee XML from database to Sales Audit.'); 
    }

    public function execute(InputInterface $input, OutputInterface $output)
    { 

        $application = new EmployeeMoveApplication();

        $application->run();
    }
}