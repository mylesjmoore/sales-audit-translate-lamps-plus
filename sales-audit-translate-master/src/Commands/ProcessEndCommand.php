<?php

namespace Lamps\Commands;

use Lamps\Database\Connection;
use Lamps\Database\Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProcessEndCommand extends Command {

    public function configure()
    {
        $this->setName('process:end')
            ->setDescription('End a process.')
            ->setHelp("
                This command will attempt to end a process with a given process id.
                The process id can be determined by running command: process:running.
                By default, it will attempt to do a soft/gracefull shutdown. 
                Provide the option -f to force a process to end. IE process:end -f <process_id>
            ")
            ->addArgument('process_id',InputArgument::REQUIRED,'The process id to end')
            ->addOption('force','f',null,'force process to end');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $query = new Query(Connection::make());

        $io = new SymfonyStyle($input, $output);
        
        $force = $input->getOption('force');

        $processId = $input->getArgument('process_id');

        if($force) 
        {
            system('kill -9 '.$processId);
            return;
        }

        if( ! posix_kill($processId,SIGTERM))
        {
            $io->error([
                "Failed to end {$processId}.",
                "Verify process ID number with sqs process:running",
                "Try sqs process:stop --force {$processId}"
            ]);
            return;
        }
        
        $io->success("Process {$processId} has ended.");
    }
}