<?php

namespace Lamps\Commands;

use Lamps\Database\Connection;
use Lamps\Database\Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProcessRunningCommand extends Command {

    public function configure()
    {
        $this->setName('process:running')
            ->setDescription('List the running processes.')
            ->setHelp("This command will list the processes currently running.");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $query = new Query(Connection::make());

        $io = new SymfonyStyle($input, $output);

        $processes = array_filter(array_map(function($array){
                if( ! in_array('grep',explode(" ",$array))) return array_filter(explode(" ",$array));
            },
            explode("\n",shell_exec("ps -eo uname,pid,pcpu,pmem,start_time,args | grep 'translate watch'"))
        ));
        
        $processes = array_map(function($array){
            
            return array_filter($array,function($string){
                if(
                    strpos($string,'translate') !== false or
                    strpos($string,'php') !== false or
                    strpos($string,'watch') !== false
                ) return false;

                return true;
            });

        },$processes);
    
        if(empty($processes)) 
        {
            $io->note('No Translate Processes Running.');
            return;
        }
    
        $io->title('Processes Running');
        
        $io->table(
            ['User','Process Id','% CPU','% MEM','Start Time','Process'],
            $processes
        );
    }
}