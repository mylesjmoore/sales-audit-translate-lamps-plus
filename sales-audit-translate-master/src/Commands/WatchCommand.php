<?php

namespace Lamps\Commands;

use Lamps\Controllers\Watcher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WatchCommand extends Command {

    public function configure()
    {
        $this->setName('watch')
            ->setDescription('Watch a process.')
            ->setHelp("This command will watch for unprocessed messages and process them.")
            ->addArgument('process_name',InputArgument::REQUIRED,'The process name to watch')
            ->setHidden(true);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $watcher = new Watcher(
            $input->getArgument('process_name')
        );

        $watcher->watch();

    }
}