<?php

namespace Lamps\Commands;

use Lamps\Controllers\Watcher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessStartCommand extends Command {

    public function configure()
    {
        $this->setName('process:start')
            ->setDescription('Start a process with a given process name.')
            ->setHelp("This command will start monitoring for unprocessed messages and process them.")
            ->addArgument('process_name',InputArgument::REQUIRED,'The table to watch');            
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $processName = $input->getArgument('process_name');
        
        exec("translate watch {$processName} >> watch.log &");
    }
}