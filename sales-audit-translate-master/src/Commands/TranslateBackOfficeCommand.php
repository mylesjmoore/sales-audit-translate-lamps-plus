<?php

namespace Lamps\Commands;

use Lamps\Application;
use Lamps\Translators\BackOfficeTranslator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TranslateBackOfficeCommand extends Command {

    public function configure()
    {
        $this->setName('translate:backoffice')
            ->setDescription('Translate pos back office transactions from json into xml.')
            ->addArgument('record_id',InputArgument::REQUIRED,'The table table record id to translate');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $application = new Application(
            new BackOfficeTranslator,
            getenv('BACK_OFFICE_TABLE'),
            $input->getArgument('record_id')
        );

        $application->run();
    }
}