<?php

namespace Lamps\Commands;

use Lamps\MoveApplication;
//use Lamps\Translators\BackOfficeTranslator;
use Symfony\Component\Console\Command\Command;
//use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoveXMLCommand extends Command {

    public function configure()
    {
        $this->setName('move:moveXML')
            ->setDescription('Move XML from database to Sales Audit.');
//            ->addArgument('record_id',InputArgument::REQUIRED,'The table table record id to translate');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

//        $backOfficeTable = getenv('BACK_OFFICE_TABLE');
// todo environment variables?

        $application = new MoveApplication();

        $application->run();
    }
}