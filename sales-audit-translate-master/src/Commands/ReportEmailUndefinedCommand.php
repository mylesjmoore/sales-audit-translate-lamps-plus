<?php

namespace Lamps\Commands;

use Lamps\Database\Connection;
use Lamps\Database\Query;
use Lamps\Email;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReportEmailUndefinedCommand extends Command {

    public function configure()
    {
        $this->setName('report:emailUndefined')
            ->setDescription('Email a list of transactions received from pos that where undefined.')
            ->setHelp("This command will email a list all undefined back office transaction message types that have not been resolved.");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $query = new Query(Connection::make());

        $errors = $query->undefinedTransactions();

        if( ! $errors) return;

        $header = "
        <html>
            <table width='900' style='border:1px solid #333'>
                <tr>
                    <td align='center'><h2><strong><font color='Red'>Json to XML Transalate Attempted to Translate Undefined Back Office Transactions</font></strong><h2></td>
                </tr> <br>

                <tr>
                    <td align='center'>
                        <table align='center' width='875' border='0' cellspacing='0' cellpadding='0' style='border:1px solid #ccc;'>
                            <tr style='padding: 10px;'>
                                <font color='Blue'>
                                <th align='center'>ID<hr></td>
                                <th align='center'>Transaction ID<hr></td>
                                <th align='center'>Process<hr></td>
                                <th align='center'>Reason<hr></td>
                                <th align='center'>Operation<hr></td>
                                <th align='center'>Status<hr></td>
                                <th align='center'>Timestamp<hr></td>
                                </font>
                            </tr>
                    
        ";

        $body = "";
        foreach($errors as $error)
        {
            $body .= "
                            <tr style='padding: 10px;'>
                                <td align='center'>{$error['JSONID']}</td>
                                <td align='center'>{$error['TRANID']}</td>
                                <td align='center'>{$error['PROCESS']}</td>
                                <td align='center'>{$error['REASON']}</td>
                                <td align='center'>{$error['OPERATION']}</td>
                                <td align='center'>{$error['RESOLVED']}</td>
                                <td align='center'>{$error['CRTDTTM']}</td>

                            </tr>
            ";
        }

        $footer = "
                        </table>
                    </td>
                </tr>
            </table>
        </html>
        ";

        Email::unprocessed($header . $body . $footer);
    }
}