<?php

namespace Lamps\Commands;

use Lamps\MoveInternetSalesApp;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoveInternetSalesCommand extends Command {

    public function configure()
    {
        $this->setName('move:inetSales')
            ->setDescription('Move & rename Inet Sales folders to PollData.');
//            ->addArgument('record_id',InputArgument::REQUIRED,'The table table record id to translate');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $application = new MoveInternetSalesApp();

        $application->run();
    }
}