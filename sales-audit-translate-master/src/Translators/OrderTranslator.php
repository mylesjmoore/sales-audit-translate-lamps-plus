<?php

namespace Lamps\Translators;

use Lamps\Transactions\OrderTransaction;

class OrderTranslator extends Translator
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * convert json into xml
     *
     * @param string $json
     * @return string
     */
    public function jsonToXml($json,$table,$recordId)
    {
        //decode json into an array
        $decodedJson = json_decode($json['data'], true);

        //create order transaction with json (constructs an array of tags needed for xml)
        $transaction = new OrderTransaction($decodedJson,$table,$recordId);

        //Check if this is valid transaction tags
        if (! $transaction->tags) return false;

        //create xml from transaction object's tags
        $xml = $this->xmlUtility->makeXmlFromArray(
            $transaction->rootTag,
            $transaction->tags
        );

        //return the xml as a string
        return $this->xmlUtility->xmlString($xml);
    }

}