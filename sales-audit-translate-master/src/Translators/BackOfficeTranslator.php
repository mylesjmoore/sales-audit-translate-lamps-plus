<?php

namespace Lamps\Translators;

use Lamps\Database\Query;
use Lamps\Database\Connection;

class BackOfficeTranslator extends Translator
{
    public function __construct()
    {
        parent::__construct();

        $this->query = new Query(Connection::make());
    }

    /**
     * convert json into xml
     *
     * @param string $json
     * @return string
     */
    public function jsonToXml($json,$table,$recordId)
    {
        //decode json into array
        $decodedJson = json_decode($json['data'], true);

        //if status is open, use void transaction, else use operation type
        $operationType = ($decodedJson['Status'] == "Open") ? "VOID_TRANSACTION" : trim($decodedJson['OperationType']);

        //Check if status is cancelled
        if($decodedJson['Status'] == "Cancelled") $operationType = "VOID_TRANSACTION";
        
        //construct transaction type
        $transactionType = "Lamps\Transactions\\{$operationType}";
        
        //if transaction is not defined
        if( ! class_exists($transactionType) )
        {
            //record the undefined transaction type
            //$this->query->recordUndefinedTransactionType($json['id'],$decodedJson['TransactionID'],$decodedJson['OperationType']);
            
            //return without xml: conversion failed
            return false;
        }

        if($operationType == 'TILL_UPDATE') $this->query->setDumpedTransactionMessage($table,$recordId,'Till Update ignored.');

        //Set Till Reconcile to hold if no EOD
        if($operationType == 'TILL_RECONCILE')
        {
            $this->query->holdTillReconcile($table,$recordId);
            $reconcileStatus = $this->query->tillReconcileStatus($table,$recordId);
            if($reconcileStatus == 'H') return false;
        }

        //create a new transaction (constructs an array of tags needed for xml)
        $transaction = new $transactionType($decodedJson,$table);
        $xml3 = null;
        
        //create xml from array data in the transaction
        $xml = $this->xmlUtility->makeXmlFromArray(
            $transaction->rootTag,
            $transaction->tags
        );

        //Needed for missing transactions in the translate
        $transactionType = "Lamps\Transactions\\VOID_TRANSACTION";
        $transaction = new $transactionType($decodedJson,$table);
        $xml2 = $this->xmlUtility->makeXmlFromArray(
            $transaction->rootTag,
            $transaction->tags
        );

        //if EOD, add in the cash management transaction
        if($operationType == 'STORE_CLOSE')
        {
            $this->query->releaseTillReconcile($table,$recordId);
            $transactionType = "Lamps\Transactions\\CASH_MANAGEMENT_SUMMARY";
            $transaction = new $transactionType($decodedJson,$table);
            $xml3 = $this->xmlUtility->makeXmlFromArray(
                $transaction->rootTag,
                $transaction->tags
            );
            $xml3 = $this->xmlUtility->xmlString($xml3);
        }
        
        //return the xml as a string, ignore duplicate voided transactions
        $missingTransactionTypes = array('TILL_OPEN','TILL_RECONCILE','BANK_DEPOSIT','STORE_OPEN','STORE_CLOSE');
        $xml1 = $this->xmlUtility->xmlString($xml);
        $xml2 = $this->xmlUtility->xmlString($xml2);
        if(!in_array($operationType, $missingTransactionTypes)) $xml2 = null;
        return $xml1 . ' ' . $xml2 . ' ' . $xml3;
    }

}