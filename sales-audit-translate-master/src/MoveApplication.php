<?php

namespace Lamps;

use Lamps\Email;
use Lamps\Log;
use Lamps\ErrorHandler;
use Lamps\Database\Query;
use Lamps\Database\Connection;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class MoveApplication
{
    protected $errorHandler;
    protected $query;

    public function __construct()
    {

        $this->query = new Query(Connection::make());

        $this->fileSystem = new Filesystem();

        //instead of environment variables - put the directories in the NOTES/config file
        $this->XMLLocation = getenv('SALES_AUDIT');

    }

    public function run()
    {
        $xmlRows = $this->query->unProcessedXML();

        $curStore = 0;
        $store = 0;
        $folderCreated = false;

        $folderDate = date("mdHi"); 
    
        $folder =  $this->XMLLocation.'.'.$folderDate;
        //as of 11/30/18 we will have store 0 XML - we shouldn't but...
        $newFile = 'XPOLLD00000001';

        while ($row = $xmlRows->fetch()) {
            try {
                $this->fileSystem->mkdir($folder,true);
            } catch (IOExceptionInterface $exception) {
                Email::error("
                    Sales Audit Translate: Move XML sales data to sales audit server: <br><br>
                    The move application attempted to move pos xml sales data to the sales audit server and failed <br><br>
                    An error occurred while creating a directory at {$exception->getPath()} <br><br>
                    This could be related to the php servers /Storage permissions. <br><br>
                    The processes has ended and has not moved the sales data.
                ");
                exit(1);
            }
            $store = $row['STORENO'];
            //start the file for a new store
            if ($curStore<>$store)
            {
                //set the file name -used to create/append
                //left pad the store no with 0 
                $storePad = str_pad($store,2,'0', STR_PAD_LEFT);
                $newFile = 'XPOLLD00'.$storePad.'0001';
                $curStore=$row['STORENO'];                
            }
            //appendToFile creates it if doesn't exist
            $this->fileSystem->appendToFile($folder.'/'.$newFile,$row['XMLCLOB']);

            $this->query->setXMLProcessed($row['JSONID'],$folderDate);
        }
        //how do I know whether to write the go file?
        //finish off with go file and renaming the folder
        if ($curStore<>0)
        {
            $this->fileSystem->touch($folder.'/'.'ReadyToGo.done');
            $this->fileSystem->rename($folder,$folder.'.IP');
        }
    }
}
