<?php

namespace Lamps;

use Lamps\ErrorHandler;
use Lamps\Database\Query;
use Lamps\Database\Connection;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class MoveLoyaltyNumberDataApplication
{
    protected $errorHandler;
    protected $query;

    public function __construct()
    {

        $this->query = new Query(Connection::make());

        $this->fileSystem = new Filesystem();

        //instead of environment variables - put the directories in the NOTES/config file
        $this->XMLLocation = getenv('SALES_DATA');
        $this->BackupLocation = getenv('SALES_DATA_BACKUP');

    }

    public function run()
    {
        //Check if Loyalty Number Data is present in the file
        $loyaltyDataExists = $this->fileSystem->exists([__DIR__.'/LoyaltyNumbers']);
        
        //Parse Loyalty Numbers Data
        if($loyaltyDataExists)
        {
            //Insert Loyalty Numbers Data to IBMi
            $file = file(__DIR__.'/LoyaltyNumbers');
            $this->query->insertLoyaltyData($file); 

            //Rename file to complete
            $this->fileSystem->rename(__DIR__.'/LoyaltyNumbers', __DIR__.'/LoyaltyNumbersComplete', true);
        }
    }
}
